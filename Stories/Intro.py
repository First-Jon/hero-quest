from Story import Story
from StoryThread import StoryThread

from Graphics.TextTypes.Dialogue import Dialogue

from City import City
import Context
from Encounter import Encounter, EncounterReturn
from EntityGroup import EntityGroup
from Hero import Hero
from Input import InputOption
from ID import ID
from ItemType.Boot import Boot
from ItemType.Chest import Chest
from ItemType.Dagger import Dagger
from Message import Message, NarrationType
from Monster import Monster

class Intro(Story):
	ID = ID.STORY_INTRO
	
	def __init__(self):
		super().__init__()
		self.name = "Intro"
		
	def start(self):
		super().start()
		Message.narrate("The sun was high in the sky as Farron worked in the field, the days here were hot and the nights were far too cold.")
		Message.narrate("But he had to work, for if he didn't weeds would come up instead of the vegetables he needed to nourish his family")
		
		Message.narrate("Suddenly he heard a cry. No, a scream rather.")
		Message.narrate("Which.. wasn't quite right. He was alone, or was he?")
		Message.narrate("The screams grew louder, climbing up from his toil, Farron went to a nearby hill to get a view.")
		Message.narrate("There! In the distance! A young girl being chased by... what?!")
		Message.narrate("He had no idea what it might be, something nasty of course, at least it looked nasty")
		Message.narrate("Courage, and the will to protect, had him running with nothing but a farming hoe.")
		Message.narrate("There was no need for daggers since most fights could be solved with an exchange of fists.")
		Message.narrate("But ohh, how he wished you at least bought a cooking knife with him.")
		
		
		dialogue = Dialogue(
			aTextsToRead = [
				"The sun was high in the sky as Farron worked in the field, the days here were hot and the nights were far too cold.",
				"But he had to work, for if he didn't weeds would come up instead of the vegetables he needed to nourish his family",
				"Suddenly he heard a cry. No, a scream rather.",
				"Which.. wasn't quite right. He was alone, or was he?",
				"The screams grew louder, climbing up from his toil, Farron went to a nearby hill to get a view.",
				"There! In the distance! A young girl being chased by... what?!",
				"He had no idea what it might be, something nasty of course, at least it looked nasty",
				"Courage, and the will to protect, had him running with nothing but a farming hoe.",
				"There was no need for daggers since most fights could be solved with an exchange of fists.",
				"But ohh, how he wished you at least bought a cooking knife with him."
			],
			aOptionsToDisplay = {
				InputOption.ATTACK : "Attack!",
				InputOption.RUN    : "Run!",
			}
		)
		
		ePlayerChoice = dialogue.chooseOption()
		dialogue.close()
		match ePlayerChoice:
			case InputOption.ATTACK:
				dialogue = Dialogue(
					aTextsToRead = [
						"Farron approached the Goblin!"
					]
				).waitTillDoneReading()
				
			case InputOption.RUN:
				print("Ran away!")
		
		
		jenni  = Hero.Generate(ID.HERO_JENNI)
		jenni.equipItem(Dagger.Generate(ID.ITEM_DAGGER_SIMPLE))
		
		farron = Hero.Generate(ID.HERO_FARRON)
		farron.equipItem(Chest.Generate(ID.ITEM_WEATHERED_LEATHER_TUNIC))
		farron.equipItem(Boot.Generate(ID.ITEM_WEATHERED_LEATHER_BOOTS))
		
		player = StoryThread.GetPlayer()
		player.team.addMember(jenni)
		player.team.addMember(farron)
		
		goblinTeam = EntityGroup("the nasty goblin")
		goblinTeam.addMember(Monster.Generate(ID.MONSTER_GOBLIN))
		
		encounter = Encounter(player.number, goblinTeam)
		
		encounter.executeTillFinished()
		
		if player.team.hasEntity(jenni):
		
		
			#Message.narrate("Thank you for saving me, another mile or so and that create would've got me")
			#Message.narrate("Let's go to the city!")
			#Message.narrate("Heading to the city, Jenni told Farron all about her journey")
			#Message.narrate("It turns out she was from the east and she had been sent to deliver a package to the province of Rigoth")
			#Message.narrate("That was two moons ago, before everything else had happened. . .")
			
			Dialogue(
				[
					"Thank you for saving me, another mile or so and that create would've got me",
					"Let's go to the city!",
					"Heading to the city, Jenni told Farron all about her journey",
					"It turns out she was from the east and she had been sent to deliver a package to the province of Rigoth",
					"That was two moons ago, before everything else had happened. . .",
				]
			)
			arigithDoman = City.Fetch(ID.CITY_ARIGITH_DOMAN)
			arigithDoman.intro()
			
			player.questLog.addQuestByID(ID.QUEST_JENNI_S_STORY)
			
			arigithDoman.enter(player)
			
		else:
			Message.narrate("Game over, Jenni has died")
			Dialogue("Game over, Jenni has died")
				
Story.Register(Intro)