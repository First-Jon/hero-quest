from Stat import StatType
from Input import Input
from Message import Message, NarrationType
from Reporter import Reporter


from Graphics.ObjPicker import ObjPicker
from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

class EntityGroup(Reporter):
	def __init__(self, description="", gold=0):
		super().__init__()
		self.members = []
		self.description = description
		self.gold = gold
		
	def addMember(self, entity):
		self.members.append(entity)
		entity.entityGroup = self
		
	def removeMember(self, entity):
		if entity in self.members:
			self.members.remove(entity)
			entity.entityGroup = None
			
	def hasEntity(self, entity):
		if entity in self.members:
			return True
		else:
			return False
			
	def clear(self):
		self.members = []
		
	def gainGold(self, gold):
		self.gold += gold
		#Message.narrate("%s has gained %d gold" % (self.description, gold))
		Dialogue("%s has gained %d gold" % (self.description, gold))
		
	def gainExperience(self, experience):
		aValidMembers = []
		for member in self.members:
			if member.info.bCanGainExperience:
				aValidMembers.append(member)
				
		iValidMemberSize = len(aValidMembers)
		if iValidMemberSize > 0:
			iExperienceForEachMember = experience / iValidMemberSize
			for member in aValidMembers:
				member.gainExperience(iExperienceForEachMember)
		
	def gainItem(self, pItem):
		#Message.narrate("Who do you want to give %s to?" % pItem.name)
		Dialogue("Who do you want to give %s to?" % pItem.name)
		pChosenMember = self.choose()
		pChosenMember.gainItem(pItem)
		
	def gainItems(self, *aItems):
		for item in aItems:
			self.gainItem(item)
		
	
		
	def checkAffordability(self, iGoldToSpend):
		if self.gold >= iGoldToSpend:
			return True
		else:
			return False
	
	def spendGold(self, iGoldToSpend):
		if self.checkAffordability(iGoldToSpend):
			self.gold -= iGoldToSpend
			#Message.narrate("You spend %d gold" % iGoldToSpend)
			Dialogue("You spend %d gold" % iGoldToSpend)
			return True
		else:
			#Message.narrate("You don't have enough gold!" % iGoldToSpend)
			Dialogue("You don't have enough gold!" % iGoldToSpend)
			return False
			
	def getCombinedAttribute(self, eAttribute):
		returnValue = 0
		
		for member in self.members:
			returnValue += member.getAttribute(eAttribute)
			
		return returnValue
		
	def getCombinedStat(self, eStat):
		returnValue = 0
		
		for member in self.members:
			returnValue += member.getStat(eStat)
			
		return returnValue

	def getCombinedInfo(self, eInfo):
		returnValue = 0
		
		for member in self.members:
			returnValue += member.info.getInfo(eInfo)
			
		return returnValue
		
	def print(self):
		for member in self.members:
			#Message.narrate(member.name, NarrationType.NEAR_INSTANT)
			Dialogue(member.name, NarrationType.NEAR_INSTANT)
			
	# Use this to help decide initiative
	def combinedFirstStrike(self):
		firstStrike = 0
		for member in self.members:
			firstStrike += member.firstStrikes
			
	def randomMember(self):
		return random.choice(self.members)
		
	def memberCount(self):
		return self.members.size
		
	def choose(self, bExitAvailable=True):
		objPicker = ObjPicker(self.members, bExitAvailable=bExitAvailable)
		chosenEntity = objPicker.openChooseAndClose()
		return chosenEntity
		#return Input.PlayerChoiceFromObjList(self.members, bExitAvailable=bExitAvailable)