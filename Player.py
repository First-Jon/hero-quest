from enum import Enum
import threading

from EntityGroup import EntityGroup
from Quest import QuestLog
from Reporter import Reporter


class PlayerData():
	aActivePlayers = {}

# For possible multiplayer in the future
class PlayerNumber(Enum):
	NONE = 0
	ONE   = 1
	TWO   = 2
	THREE = 3
	FOUR  = 4
	
class PlayerLock():
	def __init__(self, player):
		self.player = player
		self.rlock = threading.RLock()

class Player(Reporter):
	def __init__(self, number=PlayerNumber.NONE):
		self.number = number
		#self.aActiveMenus = []
		self.team = EntityGroup("Your party", gold=50)
		
		self.questLog = QuestLog(self)
		#self.previousMenuID = None
		
	def release(self):
		Player.Release(self.number)
# '''		
	# def addMenu(self, pMenu):
		# print(pMenu)
		# if pMenu not in self.aActiveMenus:
			# pMenu.owner = self
			# self.aActiveMenus.append(pMenu)
		# else:
			# # Todo, add warning, menu already in activemenus
			# pass
		
	# def removeMenu(self, pMenu):
		# if pMenu in self.aActiveMenus:
			# self.aActiveMenus.remove(pMenu)
			
		
	# def swapMenu(self, pMenuToRemove, pMenuToAdd):
		# self.removeMenu(pMenuToRemove)
		# self.addMenu(pMenuToAdd)
		
	# def updateActiveMenus(self, events):
		# for menu in self.aActiveMenus:
			# menu.update(events)
			
	# def revertToPreviousMenu(self, bRun = True):
		# self.setActiveMenu(self.previousMenuID, bRun)	
# '''	
	@staticmethod
	def Register(ePlayerNumber):
		if isinstance(ePlayerNumber, PlayerNumber):
			if ePlayerNumber != PlayerNumber.NONE:
				if ePlayerNumber not in PlayerData.aActivePlayers:
					newPlayer = Player(ePlayerNumber)
					
					newPlayerLock = PlayerLock(newPlayer)
					
					PlayerData.aActivePlayers[ePlayerNumber] = newPlayerLock
			
	@staticmethod
	def Fetch(ePlayerNumber):
		if isinstance(ePlayerNumber, PlayerNumber):
			if ePlayerNumber != PlayerNumber.NONE:
			
				if ePlayerNumber not in PlayerData.aActivePlayers:
					Player.Register(ePlayerNumber)
					
				PlayerData.aActivePlayers[ePlayerNumber].rlock.acquire()
				return PlayerData.aActivePlayers[ePlayerNumber].player
			
		else:
			return Player(ePlayerNumber)
	
	@staticmethod
	def Release(ePlayerNumber):
		if ePlayerNumber in PlayerData.aActivePlayers:
			PlayerData.aActivePlayers[ePlayerNumber].rlock.release()