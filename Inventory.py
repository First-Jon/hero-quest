from Input import Input, InputOption
from Item import ConsumableType
from Message import Message, NarrationType
from Reporter import Reporter

from Graphics.ObjPicker import ObjPicker
from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

class Inventory(Reporter):
	def __init__(self, pOwner):
		super().__init__()
		self.owner = pOwner
		self.items = []
		
	def add(self, pItem):
		pItem.parent = self
		self.items.append(pItem)
		
	def remove(self, pItem, bDestroy = False):
		if pItem in self.items:
			pItem.parent = None
			self.items.remove(pItem)
			
	def hasItems(self):
		return len(self.items) > 0

	def choose(self, bExitAvailable=True):
		return Input.PlayerChoiceFromObjList(self.items, bExitAvailable=bExitAvailable)
		
	def chooseAndUse(self, bUseOwner=True, entity=None):
		if bUseOwner:
			entity = self.owner
		else:
			if entity == None:
				pass
				# Todo, throw error
		bReadyToExit = False
		bExitValue = False
	
		while not bReadyToExit:
			#item = Input.PlayerChoiceFromObjList(self.items, bExitAvailable=True)
			item = ObjPicker(self.items, bExitAvailable=True).openChooseAndClose()
			
			if item: # If the item isn't valid, the user decided to exit
				if item.consumableType == ConsumableType.NONE:
					#Message.narrate("Equip %s?" % item.name)
					Dialogue("Equip %s?" % item.name)
				else:
					#Message.narrate("Use %s?" % item.name)
					Dialogue("Use %s?" % item.name)
					
				eUserChoice = Selector(
					{
						InputOption.YES : "'Yes'",
						InputOption.NO  : "'No'"
					}
				).openChooseAndClose()
				match eUserChoice:
					case InputOption.YES:
						item.use(entity)
						bReadyToExit = True
						bExitValue = True
						
					case InputOption.NO:
						pass # Choose a different item
				
			else:
				bReadyToExit = True
				bExitValue = False
				
		return bExitValue