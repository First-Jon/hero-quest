


class Hook():
	aActiveHooks = [] # This is seperate for each hook class
	
	def __init__(self):
		self.all = {}
		
	def __del__(self):
		if self in __class__.aActiveHooks:
			self.__class__.aActiveHooks.remove(self)
		
	def addEvent(self, event, functionToCall):
		print(event)
		print(functionToCall)
		print(self.__class__)
		self.all[event] = functionToCall
		
		# Now that this hook has an event, add it to the active hooks to begin to check it
		# This list is seperate for each hook class
		if self not in self.__class__.aActiveHooks:
			self.__class__.aActiveHooks.append(self)
		
	def checkEvent(self, event):
		if event in self.all:
			self.all[event]()
			
	@classmethod
	def checkAll(callingClass, event):
		for hook in callingClass.aActiveHooks:
			hook.checkEvent(event)