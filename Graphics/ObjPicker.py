import threading

from Graphics.Menu import Menu
from Graphics.Screen import ScreenLocation
from Graphics.UI import UIType

from ID import ID
from Input import InputOption

# Designed to be imported directly rather than retrieved through Menu.Generate(ID.MENU_OBJ_PICKER)

class ObjPicker(Menu):
	ID = ID.MENU_OBJ_PICKER
	eUIType = UIType.MENU_DIALOGUE_OPTION
	TITLE = "Pick"
	
	def __init__(self, aObjList, bIncludeDescriptions=False, bReturnIndex=False, bExitAvailable=False):
		super().__init__(ObjPicker.eUIType, ObjPicker.TITLE)
		
		self.aObjList = aObjList
		self.bIncludeDescriptions = bIncludeDescriptions
		self.bReturnIndex = bReturnIndex
		self.bExitAvailable = bExitAvailable
		
		for index, obj in enumerate(self.aObjList):
			self.menu.add.button(obj.name, self.chooseOption, index)
			
		if self.bExitAvailable:
			self.menu.add.button("Exit", self.chooseOption, -1)
			
		self.bOptionChosen = threading.Event()
		self.chosenObj = None
		self.chosenIndex = -1
			
	def chooseOption(self, index):
		# If exiting is available, and the player decided to choose that option
		if self.bExitAvailable and index == -1:
			self.chosenObj = False
		else:
			self.chosenObj = self.aObjList[index]
			
		self.chosenIndex = index
		self.bOptionChosen.set()
		
	# Waits till the bOptionChosen flag is set,
	# Once it is, returns eChosenOption
	def waitForChoice(self):
		self.bOptionChosen.wait()
		self.bOptionChosen.clear()
		
		if self.bReturnIndex:
			return self.chosenObj, self.chosenIndex
		else:
			return self.chosenObj
		
	# All in one function to open the selector, wait for an option, close, and return the option
	def openChooseAndClose(self):
		self.open()
		
		if self.bReturnIndex:
			chosenObj, chosenIndex = self.waitForChoice()
			self.close()
			return chosenObj, chosenIndex
			
		else:
			chosenObj = self.waitForChoice()
			self.close()
			return chosenObj