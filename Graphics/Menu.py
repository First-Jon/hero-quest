import pygame_menu
import pygame

from Graphics.Screen import Screen
from Graphics.UI import UI, UIType

from ID import ID


class Menu(UI):
	aRegisteredMenus = {}
	ID = ID.GENERIC_MENU
	INFO = True
	INFO_V = True
	
	def __init__(self, eUIType = UIType.NONE, title ="", bAddToScreen=True):
		super().__init__(eUIType, bAddToScreen)
		
		self.menu = pygame_menu.Menu(title, self.rect.width, self.rect.height, position=(0,0, False))

		self.previousMenuId = ID.NONE
		
	def open(self):
		Screen.Get().addMenu(self)
		
	def close(self):
		Screen.Get().removeMenu(self)
		super().close()
		
	def update(self, events):
		for event in events:
			if 'pos' in event.__dict__:
				x = event.__dict__['pos'][0] - self.rect.x
				y = event.__dict__['pos'][1] - self.rect.y
				event.__dict__['pos'] = (x, y)
		self.menu.update(events)
		self.menu.draw(self.image)
		self.mark()
		
	def switch(self, eID):
		newMenu = Menu.Generate(eID)
		newMenu.previousMenuId = self.id
		
		self.close()
		newMenu.open()
		
	def revert(self):
		newMenu = Menu.Generate(self.previousMenuId)
		newMenu.previousMenuId = self.id
		
		self.close()
		newMenu.open()
		

	@staticmethod
	def Generate(eID):
		pMenu = Menu(bAddToScreen=False)
		if eID in Menu.aRegisteredMenus:
			pMenu = Menu.aRegisteredMenus[eID]()
			Menu.info("Generating menu with ID %s" % eID)
			#Menu.info_v("Passed Owner: %s" % owner)
			Menu.info_v("pMenu: %s" % pMenu)
			#Menu.info_v("pMenu.owner: %s" % pMenu.owner)
		else:
			Menu.error("%s is not a registered menu ID!" % eID)
			pass

		return pMenu
	
	@staticmethod
	def Register(pMenuClass):
		# Make sure the provided class derives from Menu
		if Menu in pMenuClass.__bases__:
			Menu.aRegisteredMenus[pMenuClass.ID] = pMenuClass
			Menu.info("Registered menu with ID %s" % pMenuClass.ID)
				
		else:
			Menu.warning("Failed to register %s! Parent class is not 'Menu'" % pMenuClass)
			pass