from enum import Enum, auto

import pygame

from Graphics.Screen import ScreenLocation
from Graphics.Sprite import Sprite

from ID import ID

class UIType(Enum):
	NONE = auto(),
	MENU_TOP_CENTER = auto(),
	MENU_DIALOGUE_OPTION = auto(),
	
	DIALOGUE_STORY  = auto(),
	
	
	
	
class UI(Sprite):
	ID = ID.GENERIC_UI
	def __init__(self, eUIType = UIType.NONE, bAddToScreen=True):
		self.eUIType = eUIType
		self.rect = self.getRect()
		super().__init__(self.rect, bAddToScreen)
		
	def getRect(self):
		return UI.GetRect(self.eUIType)
		
	@staticmethod
	def GetRect(eUIType):
		x = 0
		y = 0
		width = 100
		height = 100
		
		match eUIType:
			case UIType.MENU_TOP_CENTER:
				width = 200
				height = 400
				
				x, y = ScreenLocation.GetAndAdjustToCenter(ScreenLocation.TOP_CENTER, width)
				y += 40
				
			case UIType.MENU_DIALOGUE_OPTION:
				width = 600
				height = 200
				
				x, y = ScreenLocation.GetAndAdjustToCenter(ScreenLocation.BOTTOM_CENTER, width)
				y -= 440
			
			case UIType.DIALOGUE_STORY:
				width = 500
				height = 200
				
				x, y = ScreenLocation.Get(ScreenLocation.BOTTOM_BETWEEN_LEFT_AND_CENTER)
				y -= 200
			
		return pygame.Rect(x, y, width, height)
		
	@staticmethod
	def DisplayAllRects():
		for eUIType in UIType:
			print(UI.GetRect(eUIType))