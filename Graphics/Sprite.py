import pygame

from Graphics.Screen import Screen, ScreenLocation
from Graphics.HookKey import HookKey

from ID import ID
from Obj import Obj

class Sprite(pygame.sprite.DirtySprite, Obj):
	WIDTH = 200
	HEIGHT = 400
	POS = ScreenLocation.TOP_CENTER
	X_OFFSET = 0
	Y_OFFSET = 20
	
	ID = ID.GENERIC_SPRITE
	
	def __init__(self, rect=pygame.Rect(0,0,0,0), bAddToScreen=True):
		super().__init__()
		
		self.image = pygame.Surface((rect.width, rect.height))
		self.image.fill(pygame.Color(0,0,0,255))
		self.rect = rect
		
		self.hookKey = HookKey()
		
		if bAddToScreen:
			Screen.Get().add(self)
			
	def __del__(self):
		Screen.Get().remove(self)
		
	def close(self):
		Screen.Get().remove(self)
		
	# Marks this sprite to be redrawn in the next update
	def mark(self):
		self.dirty = 2
		
	def setPosition(self, xy):
		self.rect.topleft = xy
		
	def setSize(self, width, height):
		self.rect.width = width
		self.rect.height = height
	
'''	
	@classmethod
	def establishRect(callingClass, self, pos, width, height):
		if width == None:
			width = callingClass.WIDTH
			
		if height == None:
			height = callingClass.HEIGHT
			
		if pos == None:
			pos = ScreenLocation.GetAndAdjustToCenter(callingClass.POS, width)
			pos = (pos[0]+callingClass.X_OFFSET, pos[1]+callingClass.Y_OFFSET)
			
		rect = pygame.Rect((pos[0], pos[1]), (width, height))
		return rect
'''