from enum import Enum
import pygame

from Graphics.Font import Font, FontType
from Graphics.Sprite import Sprite
from Graphics.UI import UI, UIType

from ID import ID


# If needed, this can have its own folder in the future like other objects
class TextType(Enum):
	NONE     = 0,
	DIALOGUE = 1,
	ITEM     = 2
	

class Text(UI):
	ID = ID.GENERIC_TEXT
	def __init__(self, eUIType=UIType.NONE, bAddToScreen=True,
				string="", eFontType=FontType.DEFAULT, size=16):
		super().__init__(eUIType, bAddToScreen)
		
		self.string = string
		self.eFontType = eFontType
		self.size = size
		
		self.render()
		
	def updateString(self, string):
		self.string = string
		
	def render(self):
		font = Font.Fetch(self.eFontType)
		self.image, auxillaryRect = font.render(self.string, (0,0,0), size=self.size)
		
		self.rect.width = auxillaryRect.width
		self.rect.height = auxillaryRect.height
		
		#self.rect = font.render_to(self.image, self.rect.topleft, self.string, (255,255,255), (0,0,0), size=self.size)
		self.mark()
		#self.image = pygame.Surface((self.rect.width, self.rect.height), 0, self.image)