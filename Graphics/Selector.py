import threading

from Graphics.Menu import Menu
from Graphics.Screen import ScreenLocation
from Graphics.UI import UIType

from ID import ID
from Input import InputOption

# Designed to be imported directly rather than retrieved through Menu.Generate(ID.MENU_SELECTOR)

class Selector(Menu):
	ID = ID.MENU_SELECTOR
	eUIType = UIType.MENU_DIALOGUE_OPTION
	TITLE = "Choose an option"
	#WIDTH = 200
	#HEIGHT = 400
	#POS = ScreenLocation.TOP_CENTER
	#Y_OFFSET = 40
	
	def __init__(self, options = {}):
		super().__init__(Selector.eUIType, Selector.TITLE)
		
		for eInputOption, text in options.items():
			self.menu.add.button(text, self.chooseOption, eInputOption)
			
		self.bOptionChosen = threading.Event()
		self.eChosenOption = InputOption.NULL
			
	def chooseOption(self, eInputOption):
		self.eChosenOption = eInputOption
		self.bOptionChosen.set()
		
	# Waits till the bOptionChosen flag is set,
	# Once it is, returns eChosenOption
	def waitForChoice(self):
		self.bOptionChosen.wait()
		self.bOptionChosen.clear()
		return self.eChosenOption
		
	# All in one function to open the selector, wait for an option, close, and return the option
	def openChooseAndClose(self):
		self.open()
		eUserChoice = self.waitForChoice()
		self.close()
		
		return eUserChoice
		