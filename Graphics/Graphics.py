import pygame
import pygame.freetype

import threading

from Graphics.Font import Font
from Graphics.Screen import Screen

class Graphics():
	FPS = 60
	Thread = None
	
	@staticmethod
	def Initialize():
		pygame.init()
		Font.Initialize()
		
		screen = Screen()
		screen.start()
		
	@staticmethod
	def Stop():
		pass