import pygame
import pygame.freetype

from enum import Enum

class FontType(Enum):
	DEFAULT = 1


class Font():
	aLoadedFonts = {}
	
	@staticmethod
	def Fetch(eFontType):
		if eFontType in Font.aLoadedFonts:
			return Font.aLoadedFonts[eFontType]
			
		else:
			# Todo, error
			pass
		
	@staticmethod
	def Load(eFontType, fontObject):
		if eFontType in Font.aLoadedFonts:
			# Todo, add warning and overwrite
			pass
			
		Font.aLoadedFonts[eFontType] = fontObject
		
	@staticmethod
	def Initialize():
		pygame.freetype.init()
		
		Font.Load(FontType.DEFAULT, pygame.freetype.Font(None))
		