import pygame
import threading

from Graphics.Font import FontType
from Graphics.Selector import Selector
from Graphics.Text import Text
from Graphics.UI import UIType


from ID import ID
from Input import InputOption
#from Player import Player, PlayerNumber
				

class Dialogue(Text):
	ID = ID.TEXTTYPE_DIALOGUE
	eUIType = UIType.DIALOGUE_STORY
	
	aActiveDialogues = {} # There can only be one dialogue for each dialogue UI space at a time
						  # Right now that means only at the UIType.DIALOGUE_STORY space
	
	def __init__(self, aTextsToRead=[], aOptionsToDisplay={}, eFontType=FontType.DEFAULT, size=16, bRead=True, bWaitTillDoneReading=True):
				
		super().__init__(Dialogue.eUIType, True, "", eFontType, size)
		
		# Texts to read needs to be a list, but the user has the option to just give a string
		if type(aTextsToRead) == list:
			self.aTextsToRead = aTextsToRead # Set it as normal
		else:
			self.aTextsToRead = [aTextsToRead] # Otherwise, if it's not a list, make sure it becomes a list

		self.aOptionsToDisplay = aOptionsToDisplay # A list of options to display after the dialogue is finished
		self.currentText = 0
		self.hookKey.addEvent(pygame.K_SPACE, self.read)
		self.bDoneReading = threading.Event()
		
		if bRead:
			self.setActive()
			self.read()
			if bWaitTillDoneReading:
				self.waitTillDoneReading()
				
	def close(self):
		if self.eUIType in Dialogue.aActiveDialogues:
			del Dialogue.aActiveDialogues[self.eUIType]
			
		super().close()
		
	def setActive(self):
		# Check if the dialogue space is occupied
		if self.eUIType in Dialogue.aActiveDialogues:
			Dialogue.aActiveDialogues[self.eUIType].close() # Close the existing one (Todo, what happens if it's waiting for an input?)
		
		Dialogue.aActiveDialogues[self.eUIType] = self # Add the new one
		
		
	# Reads the current line and advances the next one without reading it
	def read(self):
		if not self.atEnd():
			self.updateString(self.aTextsToRead[self.currentText])
			self.render()
			self.advance()
		else:
			self.bDoneReading.set()
		#else:
		#	self.chooseOption()
	
	# Reads the entire dialogue, and optionally lists options at the end
	# The return value is the option the player chose,
	# or True if no options were available
	def readAll(self, bListOptionsAtEnd=True):
		bStillTextToRead = True
		while bStillTextToRead:
		
			self.updateString(self.aTextsToRead[self.currentText])
			self.render()
			
			bStillTextToRead = self.advance()
					
		if bListOptionsAtEnd:
			return self.chooseOption()
		else:
			return True
			
			
	# Attempts to advance to the next text,
	# Returns True if successful
	# False if it's already at the last text
	def advance(self):
		if self.currentText >= len(self.aTextsToRead):
			return False
			
		else:
			self.currentText += 1
			return True
			
	def atEnd(self):
		if self.currentText >= len(self.aTextsToRead):
			return True
		else:
			return False
			
	def waitTillDoneReading(self):
		self.bDoneReading.wait()
		self.bDoneReading.clear()
		return
		
	def chooseOption(self):
		eUserChoice = InputOption.NULL
		# If we have options for the user to choose, create a selector menu
		if self.aOptionsToDisplay:
			selectorMenu = Selector(self.aOptionsToDisplay)
			eUserChoice = selectorMenu.openChooseAndClose()
			
		return eUserChoice