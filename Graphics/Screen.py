import threading

import pygame

from Graphics.HookKey import HookKey

#from Player import Player, PlayerData

# Preset screen locations, useful for placing sprites
class ScreenLocation():
	TOP_LEFT = 1
	TOP_CENTER = 2
	TOP_RIGHT = 3
	MID_LEFT = 4
	MID_CENTER = 5
	MID_RIGHT = 6
	BOTTOM_LEFT = 7
	BOTTOM_CENTER = 8
	BOTTOM_RIGHT = 9
	BOTTOM_BETWEEN_LEFT_AND_CENTER = 10
	
	RECT = pygame.Rect(0,0,0,0)
	
	# Connects the rect to be used for reference for the locations
	@staticmethod
	def Connect(rect):
		ScreenLocation.RECT = rect
		
	@staticmethod
	def Get(eScreenLocation):
		returnPos = (0, 0)
		match eScreenLocation:
			case ScreenLocation.TOP_LEFT:
				returnPos = ScreenLocation.RECT.topleft
				
			case ScreenLocation.TOP_CENTER:
				returnPos = ScreenLocation.RECT.midtop
				
			case ScreenLocation.TOP_RIGHT:
				returnPos = ScreenLocation.RECT.topright
				
			case ScreenLocation.MID_LEFT:
				returnPos = ScreenLocation.RECT.midleft
				
			case ScreenLocation.MID_CENTER:
				returnPos = ScreenLocation.RECT.center
				
			case ScreenLocation.MID_RIGHT:
				returnPos = ScreenLocation.RECT.midright
				
			case ScreenLocation.BOTTOM_LEFT:
				returnPos = ScreenLocation.RECT.bottomleft
				
			case ScreenLocation.BOTTOM_CENTER:
				returnPos = ScreenLocation.RECT.midbottom
				
			case ScreenLocation.BOTTOM_RIGHT:
				returnPos = ScreenLocation.RECT.bottomright
				
			case ScreenLocation.BOTTOM_BETWEEN_LEFT_AND_CENTER:
				x = ScreenLocation.RECT.centerx / 2
				y = ScreenLocation.RECT.bottom
				returnPos = (x, y)
				
			case _:
				pass
				# Todo, add warning
		return returnPos
		
		
	@staticmethod
	def GetAndAdjustToCenter(eScreenLocation, width=0, height=0):
		screenPosition = ScreenLocation.Get(eScreenLocation)
		
		xValue = screenPosition[0]
		if width:
			xValue -= width/2
			
		yValue = screenPosition[1]
		if height:
			yValue -= height/2
			
		return (xValue, yValue)
'''
	Screen is a singleton class, there is only meant to be one at a time.
	It's initialized in Graphics.py
	If you want to retrieve the Screen instance, please use Screen.Get()
	
'''
class Screen(threading.Thread):
	this = None
	aActivePlayers = {}
	aActiveMenus = []
	
	def __init__(self):
		if Screen.this != None:
			raise Exception("Only one instance of Screen can be initialized!")
		else:
			self.screen = pygame.display.set_mode()
			self.screen.fill((255,255,255))
			pygame.display.flip()
			self.aRectsToUpdate = []
			self.aObjectsAlive = pygame.sprite.LayeredDirty()
			self.FPS = 60
			
			super().__init__()
			self.shutdown_flag = threading.Event()
			
			ScreenLocation.Connect(self.screen.get_rect())
			Screen.this = self
			
	def run(self):
		clock = pygame.time.Clock()
		
		while not self.shutdown_flag.is_set():
			
			events = pygame.event.get()

			for event in events:
				if event.type == pygame.QUIT:
					exit()
					
				if event.type == pygame.KEYUP:
					HookKey.checkAll(event.key)

			for menu in Screen.aActiveMenus:
				menu.update(events)

			#for playerNumber in PlayerData.aActivePlayers.keys():
				
			#	Player.Fetch(playerNumber).updateActiveMenus(events)
			#	Player.Release(playerNumber)
				
			
			# Make the entire screen white to remove previous drawings, I don't know if this is the most efficient method
			# Maybe I can only fill the rects that changed?
			self.screen.fill((255,255,255)) 
			aRectsToUpdate = self.aObjectsAlive.draw(self.screen)
			#print(aRectsToUpdate)
			pygame.display.update(aRectsToUpdate)
			#pygame.display.flip()
			
			
			clock.tick(self.FPS)
			
	# Appends the sprite to the aObjectsAlive group
	def add(self, sprite):
		self.aObjectsAlive.add(sprite)
	
	# Removes a surface from the aObjectsAlive array if it's in it
	def remove(self, surface):
		if surface in self.aObjectsAlive:
			self.aObjectsAlive.remove(surface)
		else:
			#Todo, add warning
			pass
			
	def addMenu(self, menu):
		self.aActiveMenus.append(menu)
		
	def removeMenu(self, menu):
		if menu in self.aActiveMenus:
			self.aActiveMenus.remove(menu)
		else:
			# Todo, add warning
			pass
			
	# Marks a certain area to be overwritten
	def markRectForUpdate(self, rect):
		self.aRectsToUpdate.append(rect)
	
			
	@staticmethod
	def Get():
		return Screen.this
		
	@staticmethod
	def GetScreen():
		return Screen.this.screen
		
	@staticmethod
	def GetRect():
		return Screen.this.screen.get_rect()
		
		
