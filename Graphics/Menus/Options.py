from Graphics.Menu import Menu
from Graphics.Screen import ScreenLocation

import pygame_menu
import pygame

from ID import ID

class Options(Menu):
	ID = ID.MENU_OPTIONS
	TITLE = "Options"
	WIDTH = 200
	HEIGHT = 400
	POS = ScreenLocation.TOP_CENTER
	Y_OFFSET = 40
	
	def __init__(self, owner=None, pos=None, width=None, height=None):
		rect=Options.establishRect(self, pos, width, height)
		
		super().__init__(Options.TITLE, owner, rect)
		
		self.menu.add.button('Test')
		self.menu.add.button('Options')
		self.menu.add.button('Back', self.revert)
		
				
Menu.Register(Options)