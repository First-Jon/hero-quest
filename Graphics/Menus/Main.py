import pygame
import pygame_menu

from Graphics.Menu import Menu
from Graphics.Screen import ScreenLocation
from Graphics.Text import Text, TextType
from Graphics.UI import UIType

from ID import ID
from StoryThread import StoryThread

class Main(Menu):
	ID = ID.MENU_MAIN
	eUIType = UIType.MENU_TOP_CENTER
	TITLE = "Hello!"
	#WIDTH = 200
	#HEIGHT = 400
	#POS = ScreenLocation.TOP_CENTER
	#Y_OFFSET = 40
	
	def __init__(self):
		
		super().__init__(Main.eUIType, Main.TITLE)
		
		self.menu.add.button('Start', self.start)
		self.menu.add.button('Options', self.switch, ID.MENU_OPTIONS)
		self.menu.add.button('End')
		
	def start(self):
		#Text("The journey has just begun", TextType.DIALOGUE)
		self.close()
		StoryThread.Get().setStory(ID.STORY_INTRO)
				
Menu.Register(Main)