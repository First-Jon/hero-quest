from Quest import Quest, QuestStep, Task

from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

import Context
from Entity import EntityID
from ID import ID
from Loot import Loot
from Merchant import Merchant
from Message import Message, NarrationType

class JennisStory(Quest):
	ID = ID.QUEST_JENNI_S_STORY
	
	def __init__(self):
		super().__init__()
		
		self.name = "Jenni's Story"
		self.description = "Farron saved Jenni who was being attacked by goblins. Apparently she was doing an important delivery."
		self.reward = Loot.Generate(ID.REWARD_JENNI_S_STORY)
		
		self.addSteps (
			# 0
			QuestStep
			(
				"Enter Arigith Doman",
				Task(
					Context.Context(Context.Action.ENTER, ID.ANY, ID.ANY, ID.CITY_ARIGITH_DOMAN)
				),
			),
			# 1
			QuestStep
			(
				"Take Jenni to the Inn",
				Task(
					Context.Context(Context.Action.ENTER, ID.HERO_FARRON,  ID.ANY, ID.LOCATION_INN)
				),
				Task(
					Context.Context(Context.Action.ENTER, ID.HERO_JENNI, ID.ANY, ID.LOCATION_INN)
				)
			),
			# 2
			QuestStep
			(
				"Hear Jenni's story"
			),
			# 3
			QuestStep
			(
				bQuestComplete = True
			)
		)
		
	def resolveStep(self, iStep):
		super().resolveStep(iStep)
		
		match iStep:
			case 0:
				Dialogue(
					[
						"Farron and Jenni walked down the streets of Arigith Doman",
						"It wasn't a big town, a couple of shops, an inn, with everything else being residential",
						"Farron> I should take you to the inn"
					]
				)
			case 1:
				Dialogue(
					[
						"Farron went to the inn-keeper",
						"Higor> Hoy! Farron! You're early today. And look who you brought with you",
						"/Jenni Smiled faintly/",
						"Farron> Just the usual Higor, and a bowl of hot soup for the girl",
						"/Farron tossed Higor a coin/",
						"Farron and Jenni went to a table in the corner",
						"Farron> Now.. tell me why in the world that things was chasing you",
						"Over hot soup and mulled wine Jenni told Farron about her journey.",
						"Of course, she left out key parts",
						". . ."
					]
				)

	def advanceStep(self):
		super().advanceStep()
		
		match self.iCurrentStep:
			case 2:
				
				self.advanceStep()

		
				
				
Quest.Register(JennisStory)