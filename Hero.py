from enum import Enum

from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

from Attribute import AttributeType
from Entity import Entity
from Equipment import EquipSlot
from ID import ID
from Info import InfoType
from Input import Input, InputOption
from Message import Message, NarrationType
	
# All player controlled entities should be a hero
	
class Hero(Entity):
	aRegisteredHeroes = {}
	ID = ID.GENERIC_HERO
	def __init__(self):
		super().__init__()
		self.playerControlled = True
		self.info.bCanLevelUp = True
		self.info.attributePoints = 5
		
	def levelUp(self):
		if self.info.bCanLevelUp:
			super().levelUp()
			
			self.stat.health_base += 5
			self.stat.health_current += 5
			self.info.adjust(InfoType.ATTRIBUTE_POINTS, +1)
			
			#Message.narrate("%s is now level %d!" % (self.name, self.info.level))
			Dialogue("%s is now level %d!" % (self.name, self.info.level))
			self.assignAttributePoints()
			
	def assignAttributePoints(self):
		if self.info.attributePoints > 0:
			#Message.narrate("%s" % AttributeType.GetAllDescriptions(), NarrationType.INSTANT)
			Dialogue("%s" % AttributeType.GetAllDescriptions())
			
			bReadyToExit = False
			while not bReadyToExit:
				#Message.narrate("Which attribute do you want to increase? (you have %d attribute points)\n" % self.info.attributePoints)
				Dialogue("Which attribute do you want to increase? (you have %d attribute points)\n" % self.info.attributePoints)
				eUserChoice = Selector(
					{
						InputOption.AGILITY   : "'Agility'",
						InputOption.INTELLECT : "'Intellect'",
						InputOption.ENDURANCE : "'Endurance'",
						InputOption.STRENGTH  : "'Strength'",
						InputOption.EXIT      : "'Exit' spend the points later",
					}
				).openChooseAndClose()
				
				match eUserChoice:
					case InputOption.AGILITY:
						self.attribute.agility += 1
						#Message.narrate("%s's agility is now %d" % (self.name, self.attribute.agility))
						Dialogue("%s's agility is now %d" % (self.name, self.attribute.agility))
						
					case InputOption.INTELLECT:
						self.attribute.intellect += 1
						#Message.narrate("%s's intellect is now %d" % (self.name, self.attribute.intellect))
						Dialogue("%s's intellect is now %d" % (self.name, self.attribute.intellect))
						
					case InputOption.ENDURANCE:
						self.attribute.endurance += 1
						#Message.narrate("%s's endurance is now %d" % (self.name, self.attribute.endurance))
						Dialogue("%s's endurance is now %d" % (self.name, self.attribute.endurance))
						
					case InputOption.STRENGTH:
						self.attribute.strength += 1
						#Message.narrate("%s's strength is now %d" % (self.name, self.attribute.strength))
						Dialogue("%s's strength is now %d" % (self.name, self.attribute.strength))
						
					case InputOption.EXIT:
						bReadyToExit = True
						
				# If the user didn't exit, they must've spent an attribute point
				if eUserChoice != InputOption.EXIT:
					self.info.adjust(InfoType.ATTRIBUTE_POINTS, -1)
					if self.info.attributePoints <= 0:
						#Message.narrate("%s has spent all their attribute points" % self.name)
						Dialogue("%s has spent all their attribute points" % self.name)
						bReadyToExit = True
		else:
			#Message.narrate("%s doesn't have any attribute points to spend" % self.name)
			Dialogue("%s doesn't have any attribute points to spend" % self.name)
			
		
	def gainItem(self, pItem, bAskToEquipIfPossible=True):
		#Message.narrate("%s has gained %s" % (self.name, pItem.name))
		Dialogue("%s has gained %s" % (self.name, pItem.name))
		
		if bAskToEquipIfPossible and pItem.equipSlot != EquipSlot.NONE:
			#Message.narrate("Equip it?")
			Dialogue("Equip it?")
			eUserChoice = Selector(
				{
					InputOption.YES : "'Yes' equip the item",
					InputOption.NO  : "'No' just store it in the inventory"
				}
			).openChooseAndClose()
			
			match eUserChoice:
				case InputOption.YES:
					self.equipItem(pItem)
				case InputOption.NO:
					super().gainItem(pItem)
		else:
			super().gainItem(pItem)
			
	def gainExperience(self, experience):
		if self.info.bCanGainExperience:
			#Message.narrate("%s has gained %d experience" % (self.name, experience))
			Dialogue("%s has gained %d experience" % (self.name, experience))
			super().gainExperience(experience)
	
	def equipItem(self, pItem):
		super().equipItem(pItem)
		#Message.narrate("%s equipped the %s" % (self.name, pItem.name))
		Dialogue("%s equipped the %s" % (self.name, pItem.name))
		
	@staticmethod
	def Generate(eID):
		pHero = Hero()
		if eID in Hero.aRegisteredHeroes:
			pHero = Hero.aRegisteredHeroes[eID]()
			Hero.info("Generating hero with ID %s" % eID)
			Hero.info_v("pHero: %s" % pHero)
		else:
			Hero.error("%s is not a registered hero ID!" % eID)

		pHero.heal(bToFull=True, bIncludeMessage=False)
		return pHero
		
	@staticmethod
	def Register(pHeroClass):
		# Make sure the provided class derives from Hero
		if Hero in pHeroClass.__bases__:
			Hero.aRegisteredHeroes[pHeroClass.ID] = pHeroClass
			Hero.info("Registered hero with ID %s" % pHeroClass.ID)
				
		else:
			Hero.warning("Failed to register %s! Parent class is not 'Hero'" % pHeroClass)