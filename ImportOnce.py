

# Designed to be imported only from HeroQuest.py
# Includes things such as individual cities, loots, monsters, etc.
# Import statements with the * should only ever be imported once

from Cities import *

from LootSets import *

from Stories import *

from Quests import *

from Merchants import *

from Monsters import *

from Heroes import *

from ItemType.Boots import *
from ItemType.Bows import *
from ItemType.Chests import *
from ItemType.Consumables import *
from ItemType.Daggers import *
from ItemType.Helmets import *
from ItemType.Staffs import *
from ItemType.Swords import *

from Graphics.Menus import *