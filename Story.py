from enum import Enum

from ID import ID
from Obj import Obj

class Story(Obj):
	aRegisteredStories = {}
	ID = ID.GENERIC_STORY
	
	INFO   = False
	INFO_V = False
	
	def __init__(self, name=""):
		super().__init__()
		self.name = name
		self.bStarted = False
		
	def start(self):
		self.bStarted = True
		
	@staticmethod
	def Generate(eID):
		pStory = Story()
		if eID in Story.aRegisteredStories:
			pStory = Story.aRegisteredStories[eID]()
			Story.info("Generating story with ID %s" % eID)
			Story.info_v("pStory: %s" % pStory)
		else:
			Story.error("%s is not a registered story ID!" % eID)

		return pStory
		
		
	@staticmethod
	def Register(pStoryClass):
		# Make sure the provided class derives from Story
		if Story in pStoryClass.__bases__:
			Story.aRegisteredStories[pStoryClass.ID] = pStoryClass
			Story.info("Registered story with ID %s" % pStoryClass.ID)
				
		else:
			Story.warning("Failed to register %s! Parent class is not 'Story'" % pStoryClass)