from ItemType.Dagger import Dagger

from ID import ID

class SimpleDagger(Dagger):
	ID = ID.ITEM_DAGGER_SIMPLE
	def __init__(self):
		super().__init__()
		self.name = "Simple Dagger"
		self.durability = 30
		self.stat.damage = 2
		
Dagger.Register(SimpleDagger)