from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Chest(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_CHEST
	def __init__(self):
		super().__init__()
		
		# Set values most chests have
		self.eItemType = ItemType.CHEST
		self.equipSlot = EquipSlot.CHEST
		
	@staticmethod
	def Generate(eID):
		pChest = Chest()
		if eID in Chest.aRegisteredItems:
			pChest = Chest.aRegisteredItems[eID]()
			Chest.info("Generating chest with ID %s" % eID)
			Chest.info_v("pChest: %s" % pChest)
		else:
			Chest.error("%s is not a registered chest ID!" % eID)
				
		return pChest
		
	@staticmethod
	def Register(pChestClass):
		# Make sure the provided class derives from Chest
		if Chest in pChestClass.__bases__:
			Chest.aRegisteredItems[pChestClass.ID] = pChestClass
			Chest.info("Registered chest with ID %s" % pChestClass.ID)
				
		else:
			Chest.warning("Failed to register %s! Parent class is not 'Chest'" % pChestClass)