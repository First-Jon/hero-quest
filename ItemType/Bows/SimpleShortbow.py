from ItemType.Bow import Bow

from ID import ID

class SimpleShortbow(Bow):
	ID = ID.ITEM_SIMPLE_SHORTBOW
	def __init__(self):
		super().__init__()
		self.name = "Simple Shortbow"
		self.durability = 35
		self.stat.damage = 2
		
Bow.Register(SimpleShortbow)