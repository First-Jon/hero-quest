from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Consumable(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_CONSUMABLE
	def __init__(self):
		super().__init__()
		
		# Set values most consumables have
		self.eItemType = ItemType.CONSUMABLE
		self.consumable = 1
		
	@staticmethod
	def Generate(eID):
		pConsumable = Consumable()
		if eID in Consumable.aRegisteredItems:
			pConsumable = Consumable.aRegisteredItems[eID]()
			Consumable.info("Generating consumable with ID %s" % eID)
			Consumable.info_v("pConsumable: %s" % pConsumable)
		else:
			Consumable.error("%s is not a registered consumable ID!" % eID)
				
		return pConsumable
		
	@staticmethod
	def Register(pConsumableClass):
		# Make sure the provided class derives from Consumable
		if Consumable in pConsumableClass.__bases__:
			Consumable.aRegisteredItems[pConsumableClass.ID] = pConsumableClass
			Consumable.info("Registered consumable with ID %s" % pConsumableClass.ID)
				
		else:
			Consumable.warning("Failed to register %s! Parent class is not 'Consumable'" % pConsumableClass)