from ItemType.Consumable import Consumable

from ID import ID
from Item import ConsumableType

class ModerateHealingPotion(Consumable):
	ID = ID.ITEM_MODERATE_HEALING_POTION
	def __init__(self):
		super().__init__()
		self.name = "Moderate Healing Potion"
		self.durability = 1
		self.stat.healing = 30
		self.consumableType = ConsumableType.DRINK
		
Consumable.Register(ModerateHealingPotion)