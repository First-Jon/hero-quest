from ItemType.Consumable import Consumable

from ID import ID
from Item import ConsumableType

class WeakHealingPotion(Consumable):
	ID = ID.ITEM_WEAK_HEALING_POTION
	def __init__(self):
		super().__init__()
		self.name = "Weak Healing Potion"
		self.durability = 1
		self.stat.healing = 12
		self.consumableType = ConsumableType.DRINK
		
Consumable.Register(WeakHealingPotion)