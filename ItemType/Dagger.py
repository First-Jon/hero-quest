from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Dagger(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_DAGGER
	def __init__(self):
		super().__init__()
		
		# Set values most daggers have
		self.eItemType = ItemType.DAGGER
		self.equipSlot = EquipSlot.HAND_EITHER
		self.stat.multiStrikeChance = 0.25
		
	@staticmethod
	def Generate(eID):
		pDagger = Dagger()
		if eID in Dagger.aRegisteredItems:
			pDagger = Dagger.aRegisteredItems[eID]()
			Dagger.info("Generating dagger with ID %s" % eID)
			Dagger.info_v("pDagger: %s" % pDagger)
		else:
			Dagger.error("%s is not a registered dagger ID!" % eID)
				
		return pDagger
		
	@staticmethod
	def Register(pDaggerClass):
		# Make sure the provided class derives from Dagger
		if Dagger in pDaggerClass.__bases__:
			Dagger.aRegisteredItems[pDaggerClass.ID] = pDaggerClass
			Dagger.info("Registered dagger with ID %s" % pDaggerClass.ID)
				
		else:
			Dagger.warning("Failed to register %s! Parent class is not 'Dagger'" % pDaggerClass)