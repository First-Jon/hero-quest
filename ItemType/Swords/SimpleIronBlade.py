from ItemType.Sword import Sword

from ID import ID

class SimpleIronBlade(Sword):
	ID = ID.ITEM_SIMPLE_IRON_BLADE
	def __init__(self):
		super().__init__()
		self.name = "Simple Iron Blade"
		self.durability = 40
		self.stat.damage = 4
		
Sword.Register(SimpleIronBlade)