from enum import Enum



class ItemType(Enum):
	NONE = 0
	
	SWORD = 100
	
	DAGGER = 200
	
	BOW = 300
	
	CONSUMABLE = 400
	
	HELMET = 500
	
	CHEST = 600
	
	BOOTS = 700
	
	STAFF = 800
	
	
class ItemTypeAction():

	aItemTypesThatCanParry = [
		ItemType.SWORD,
		ItemType.STAFF
	]
	
	aItemTypesThatCanBlock = [
		ItemType.SWORD,
		ItemType.STAFF
	]
	
	@staticmethod
	def CanParry(eItemType):
		if eItemType in ItemTypeAction.aItemTypesThatCanParry:
			return True
		else:
			return False
			
	@staticmethod
	def CanBlock(eItemType):
		if eItemType in ItemTypeAction.aItemTypesThatCanBlock:
			return True
		else:
			return False