from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Bow(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_BOW
	def __init__(self):
		super().__init__()
		
		# Set values most bows have
		self.eItemType = ItemType.BOW
		self.equipSlot = EquipSlot.HAND_BOTH
		self.stat.firstStrikes = 1
		
	@staticmethod
	def Generate(eID):
		pBow = Bow()
		if eID in Bow.aRegisteredItems:
			pBow = Bow.aRegisteredItems[eID]()
			Bow.info("Generating bow with ID %s" % eID)
			Bow.info_v("pBow: %s" % pBow)
		else:
			Bow.error("%s is not a registered bow ID!" % eID)
				
		return pBow
		
	@staticmethod
	def Register(pBowClass):
		# Make sure the provided class derives from Bow
		if Bow in pBowClass.__bases__:
			Bow.aRegisteredItems[pBowClass.ID] = pBowClass
			Bow.info("Registered bow with ID %s" % pBowClass.ID)
				
		else:
			Bow.warning("Failed to register %s! Parent class is not 'Bow'" % pBowClass)