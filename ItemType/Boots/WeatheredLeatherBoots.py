from ItemType.Boot import Boot

from ID import ID

class WeatheredLeatherBoots(Boot):
	ID = ID.ITEM_WEATHERED_LEATHER_BOOTS
	def __init__(self):
		super().__init__()
		self.name = "Weathered Leather Boots"
		self.stat.armor = 1
		self.durability = 10
		
Boot.Register(WeatheredLeatherBoots)