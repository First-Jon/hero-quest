from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Boot(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_BOOT
	
	def __init__(self):
		super().__init__()
		
		# Set values most boots have
		self.eItemType = ItemType.BOOTS
		self.equipSlot = EquipSlot.HEAD
		
	@staticmethod
	def Generate(eID):
		pBoot = Boot()
		if eID in Boot.aRegisteredItems:
			pBoot = Boot.aRegisteredItems[eID]()
			Boot.info("Generating boot with ID %s" % eID)
			Boot.info_v("pBoot: %s" % pBoot)
		else:
			Boot.error("%s is not a registered boot ID!" % eID)
				
		return pBoot
		
	@staticmethod
	def Register(pBootClass):
		# Make sure the provided class derives from Boot
		if Boot in pBootClass.__bases__:
			Boot.aRegisteredItems[pBootClass.ID] = pBootClass
			Boot.info("Registered boot with ID %s" % pBootClass.ID)
				
		else:
			Boot.warning("Failed to register %s! Parent class is not 'Boot'" % pBootClass)