from ItemType.Staff import Staff

from ID import ID

class WalkingStick(Staff):
	ID = ID.ITEM_WALKING_STICK
	def __init__(self):
		super().__init__()
		self.name = "Walking Stick"
		self.stat.damage = 1
		self.durability = 5
		
Staff.Register(WalkingStick)