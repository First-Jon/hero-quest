from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Sword(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_SWORD
	def __init__(self):
		super().__init__()
		
		# Set values most swords have
		self.eItemType = ItemType.SWORD
		self.equipSlot = EquipSlot.HAND_MAIN
		self.bCanParry = True
		
	@staticmethod
	def Generate(eID):
		pSword = Sword()
		if eID in Sword.aRegisteredItems:
			pSword = Sword.aRegisteredItems[eID]()
			Sword.info("Generating sword with ID %s" % eID)
			Sword.info_v("pSword: %s" % pSword)
		else:
			Sword.error("%s is not a registered sword ID!" % eID)
				
		return pSword
		
	@staticmethod
	def Register(pSwordClass):
		# Make sure the provided class derives from Sword
		if Sword in pSwordClass.__bases__:
			Sword.aRegisteredItems[pSwordClass.ID] = pSwordClass
			Sword.info("Registered sword with ID %s" % pSwordClass.ID)
				
		else:
			Sword.warning("Failed to register %s! Parent class is not 'Sword'" % pSwordClass)