from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Helmet(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_HELMET
	def __init__(self):
		super().__init__()
		
		# Set values most helmets have
		self.eItemType = ItemType.HELMET
		self.equipSlot = EquipSlot.HEAD
		
	@staticmethod
	def Generate(eID):
		pHelmet = Helmet()
		if eID in Helmet.aRegisteredItems:
			pHelmet = Helmet.aRegisteredItems[eID]()
			Helmet.info("Generating helmet with ID %s" % eID)
			Helmet.info_v("pHelmet: %s" % pHelmet)
		else:
			Helmet.error("%s is not a registered helmet ID!" % eID)
				
		return pHelmet
		
	@staticmethod
	def Register(pHelmetClass):
		# Make sure the provided class derives from Helmet
		if Helmet in pHelmetClass.__bases__:
			Helmet.aRegisteredItems[pHelmetClass.ID] = pHelmetClass
			Helmet.info("Registered helmet with ID %s" % pHelmetClass.ID)
				
		else:
			Helmet.warning("Failed to register %s! Parent class is not 'Helmet'" % pHelmetClass)