from ItemType.Chest import Chest

from ID import ID

class WeatheredLeatherTunic(Chest):
	ID = ID.ITEM_WEATHERED_LEATHER_TUNIC
	def __init__(self):
		super().__init__()
		self.name = "Weathered Leather Tunic"
		self.stat.armor = 2
		self.durability = 10
		
Chest.Register(WeatheredLeatherTunic)