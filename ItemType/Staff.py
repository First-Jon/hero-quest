from Item import Item

from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType

class Staff(Item):
	aRegisteredItems = {}
	ID = ID.GENERIC_STAFF
	def __init__(self):
		super().__init__()
		
		# Set values most staffs have
		self.eItemType = ItemType.STAFF
		self.equipSlot = EquipSlot.HAND_BOTH
		self.bCanParry = True
		
	@staticmethod
	def Generate(eID):
		pStaff = Staff()
		if eID in Staff.aRegisteredItems:
			pStaff = Staff.aRegisteredItems[eID]()
			Staff.info("Generating staff with ID %s" % eID)
			Staff.info_v("pStaff: %s" % pStaff)
		else:
			Staff.error("%s is not a registered staff ID!" % eID)
				
		return pStaff
		
	@staticmethod
	def Register(pStaffClass):
		# Make sure the provided class derives from Staff
		if Staff in pStaffClass.__bases__:
			Staff.aRegisteredItems[pStaffClass.ID] = pStaffClass
			Staff.info("Registered staff with ID %s" % pStaffClass.ID)
				
		else:
			Staff.warning("Failed to register %s! Parent class is not 'Staff'" % pStaffClass)