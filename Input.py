import Cursor
from enum import Enum

from Message import Message, NarrationType

class InputOption():
	NULL = "NULL"
	YES  = "yes"
	NO   = "no"
	
	SEARCH = "search"
	SHOP   = "shop"
	REST   = "rest"
	MANAGE = "manage"
	CHECK  = "check"
	END    = "end"
	
	PASS   = "pass"
	RETURN = "return"
	
	ATTACK  = "attack"
	USE     = "use"
	ANALYZE = "analyze"
	RUN     = "run"
	
	AGILITY = "agility"
	INTELLECT = "intellect"
	ENDURANCE = "endurance"
	STRENGTH = "strength"
	
	CHANGE = "change"
	LEAVE  = "leave"
	EXIT   = "exit"
	
	ASSIGN = "assign"
	VIEW   = "view"

class Input():

	# Todo, return InputOption.NULL when the player exits
	@staticmethod
	def PlayerChoiceFromObjList(aList, bIncludeDescriptions=False, bReturnIndex=False, bExitAvailable=False):
		i = 0
		for thing in aList:
			i +=1
			Message.narrate("[%d] %s" % (i, thing.name), NarrationType.NEAR_INSTANT)
			if bIncludeDescriptions:
				Message.narrate("%s" % thing.getDescription(), NarrationType.NEAR_INSTANT)
			
		if bExitAvailable:
			Message.narrate("\n'exit' to return", NarrationType.NEAR_INSTANT)
			
		Cursor.show_cursor()
		playerInput = 0
		while playerInput <= 0 or playerInput > i:
			playerInput = input("> ")

			# If the player is allowed to exit, first check if that's what they want
			if bExitAvailable:
				if playerInput.lower() == "exit":
					if bReturnIndex:
						return False, -1 # If bReturnIndex is True, two variables need to be returned
					else:
						return False
					
			# Make sure player input is a valid number before converting
			# and if it's not, set it to -1 and ask again
			if playerInput.isdigit():
				playerInput = int(playerInput)
			else:
				playerInput = -1

			
		index = playerInput - 1
			
		if bReturnIndex:
			return aList[index], index # If bReturnIndex is True, two variables need to be returned
		else:
			return aList[index]
	
				
	@staticmethod
	def PlayerChoiceFromInputOptions(aOptions):
		
		for i, option in enumerate(aOptions.values(), 1):
			Message.narrate("\t[%d] %s" % (i, option.title()), NarrationType.NEAR_INSTANT)
			
		userInput = ""
		bReadyToContinue = False
		selectedOption = InputOption.NULL
		
		Cursor.show_cursor()
		while (selectedOption == InputOption.NULL):
			userInput = input("> ")
			
			# User selected an option based on number
			if userInput.isdigit():
				userInput = int(userInput)
				
				# Fetch the selected option if it's within range
				if userInput > 0 and userInput <= i:
					selectedOption = list(aOptions.keys())[userInput - 1]
					
			else:
				if userInput.lower() in aOptions.keys():
					selectedOption = userInput.lower()
					
		return selectedOption