from Merchant import Merchant, MerchantItem

from ItemType.Consumable import Consumable

from ID import ID

class JerrilsHerbsAndMore(Merchant):
	ID = ID.MERCHANT_JERRILS_HERBS_AND_MORE
	
	def __init__(self):
		super().__init__()
		self.name = "Jerril's Herbs and More"
				
		self.introduction = "Welcome to Jerril's Herbs and More! The finest ingredients for all your needs."
		self.aMerchantItemsToSell = [
			MerchantItem(
				Consumable.Generate(ID.ITEM_WEAK_HEALING_POTION),
				cost=10,
				amount=1
			),
			MerchantItem(
				Consumable.Generate(ID.ITEM_WEAK_HEALING_POTION),
				cost=10,
				amount=1
			),
			MerchantItem(
				Consumable.Generate(ID.ITEM_WEAK_HEALING_POTION),
				cost=10,
				amount=1
			),
			MerchantItem(
				Consumable.Generate(ID.ITEM_MODERATE_HEALING_POTION),
				cost=30,
				amount=1
			),
		]
				
Merchant.Register(JerrilsHerbsAndMore)