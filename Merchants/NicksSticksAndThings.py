from Merchant import Merchant, MerchantItem

from ItemType.Bow import Bow
from ItemType.Staff import Staff

from ID import ID

class NicksSticksAndThings(Merchant):
	ID = ID.MERCHANT_NICKS_STICKS_AND_THINGS
	
	def __init__(self):
		super().__init__()
		self.name = "Nick's Sticks and Things"
		self.introduction = "Welcome to Nick's Sticks and Things! I have a stick to help you in the nick of time!"
		self.aMerchantItemsToSell = [
			MerchantItem(
				Staff.Generate(ID.ITEM_WALKING_STICK),
				cost=8,
				amount=1
			),
			MerchantItem(
				Bow.Generate(ID.ITEM_SIMPLE_SHORTBOW),
				cost=15,
				amount=1
			)
		]
				
Merchant.Register(NicksSticksAndThings)