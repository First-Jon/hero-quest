
from Message import Message

# Helps classes send important messages to the console
class Reporter(object):
	INFO    = False
	INFO_V  = False
	WARNING = True
	ERROR   = True

	@classmethod
	def info(callingClass, message):
		if callingClass.INFO:
			Message.info(message)
			
	@classmethod
	def info_v(callingClass, message):
		if callingClass.INFO_V:
			Message.info_v(message)
			
	@classmethod
	def warning(callingClass, message):
		if callingClass.WARNING:
			Message.warning(message)
			
	@classmethod
	def error(callingClass, message):
		if callingClass.ERROR:
			Message.error(message)