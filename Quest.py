
from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

'''
	Each quest can have multiple steps,
	and each step can have multiple tasks.


	Tasks are solved either by listening for specific flags,
	or for when a player deos an action with a specific context and values
	
	ex.
	
	Quest Kill Goblins
		Step 1:
			TasksToComplete = [
				Task:
					Context = Context.Combat.Kill
					Value   = MonsterType.GOBLIN
					Amount  = 8
			]
			
		Step 2:
			TasksToComplete = [
				Task:
					
			]
'''
from enum import Enum

from Context import Context
from ID import ID
from Loot import Loot
from Message import Message

from Obj import Obj

class Task():
	def __init__(self, context=Context(), amountNeeded=1):
		self.context = context
		self.amountNeeded  = amountNeeded
		self.amountCurrent = 0
		
	def checkContext(self, context, amount=1):
		if self.context.matches(context):
			self.amountCurrent += amount
			return True
		else:
			return False
			
	def isComplete(self):
		if self.amountCurrent >= self.amountNeeded:
			return True
		else:
			return False
		

class QuestStep():
	def __init__(self, description="", *tasks, bQuestComplete=False):
		self.description = description
		self.tasks = tasks
		self.bQuestComplete = bQuestComplete
		
	def checkContext(self, context, amount=1):
		for task in self.tasks:
			task.checkContext(context, amount)
		
	def allTasksComplete(self):
		for task in self.tasks:
			if not task.isComplete():
				return False
				
		return True
		
	def getDescription(self):
		return self.description
		
class Quest(Obj):
	INFO = False
	INFO_V = False
	WARNING = True
	ERROR = True
	
	aRegisteredQuests = {}
	ID = ID.GENERIC_QUEST
	
	def __init__(self):
		super().__init__()
		self.name = ""
		self.description = ""
		self.aSteps = []
		self.iCurrentStep = -1
		
		self.reward = Loot()
		
		self.parent = None
		
	def getDescription(self):
		return self.name
		
	def listRequirements(self):
		for i, step in enumerate(self.aSteps):
			#Message.narrate("  %d: %s" % (i+1, step.getDescription))
			Dialogue("  %d: %s" % (i+1, step.getDescription))
			
	def getQuestStep(self, iStep):
		if iStep < 0 or iStep > len(self.aSteps):
			Quest.ERROR("Could not fetch step %d for quest %s!" % (iStep, self.ID))
			return None
			
		else:
			return self.aSteps[iStep]
			
	def getCurrentQuestStep(self):
		return self.getQuestStep(self.iCurrentStep)
		
		
	def getStepDescription(self, iStep):
		step = self.getQuestStep(iStep)
		return step.description
		
	def getCurrentStepDescription(self):
		return self.getStepDescription(self.iCurrentStep)
		
		
	def checkContext(self, context, amount=1):
		currentStep = self.getCurrentQuestStep()
		currentStep.checkContext(context, amount)
		
		if currentStep.allTasksComplete():
			self.advanceStep()
		
	def addStep(self, step):
		self.aSteps.append(step)
	def addSteps(self, *steps):
		for step in steps:
			self.addStep(step)
		
	def advanceStep(self):
		self.resolveStep(self.iCurrentStep)
		
		self.iCurrentStep += 1
		currentStep = self.getCurrentQuestStep()
		if currentStep.bQuestComplete:
			self.completeQuest()
		else:
			#Message.narrate("Step %d: %s" % (self.iCurrentStep+1, self.getCurrentStepDescription()))
			Dialogue("Step %d: %s" % (self.iCurrentStep+1, self.getCurrentStepDescription()))
			
	# The actions to take when a step is completed
	def resolveStep(self, iStep):
		pass
		
	def completeQuest(self):
		#Message.narrate("Quest completed: %s" % self.name)
		Dialogue("Quest completed: %s" % self.name)
		self.parent.completeQuest(self)
		
	@staticmethod
	def Generate(eID):
		pQuest = Quest()
		
		if eID in Quest.aRegisteredQuests:
			pQuest = Quest.aRegisteredQuests[eID]()
			Quest.info("Generating quest with ID %s" % eID)
			Quest.info_v("pQuest: %s" % pQuest)
		else:
			Quest.error("%s is not a registered quest ID!" % eID)
				
		return pQuest
		
	@staticmethod
	def Register(pQuestClass):
		# Make sure the provided class derives from Quest
		if Quest in pQuestClass.__bases__:
			Quest.aRegisteredQuests[pQuestClass.ID] = pQuestClass
			Quest.info("Registered quest with ID %s" % pQuestClass.ID)
				
		else:
			Quest.warning("Failed to register %s! Parent class is not 'Quest'" % pQuestClass)
	
	
class QuestLog(Obj):
	INFO = False
	INFO_V = False
	WARNING = True
	ERROR = True

	def __init__(self, parent):
		super().__init__()
		self.parent = parent
		
		self.aActiveQuests    = {}
		self.aCompletedQuests = {}
		self.aQuestsMarkedForRemoval = []
		
	def narrateNewQuest(self, quest):
		#Message.narrate("New quest: %s" % quest.name)
		Dialogue("New quest: %s" % quest.name)
		quest.parent = self
		quest.advanceStep()
		
	def addQuestByID(self, eID):
		if eID not in self.aActiveQuests.keys():
			potentialQuest = Quest.Generate(eID)
			if potentialQuest != None:
				self.aActiveQuests[eID] = potentialQuest
				QuestLog.info("Adding quest with ID %s" % potentialQuest.ID)
				
				self.narrateNewQuest(potentialQuest)
				
			else:
				QuestLog.error("Failed to add quest with id %s" % eID)
					
		else:
			#Message.narrate("You already have the quest, %s" % quest.name)
			Dialogue("You already have the quest, %s" % quest.name)
			
			
	def addQuest(self, quest):
		if quest.ID not in self.aActiveQuests.keys():
			self.aActiveQuests[quest.ID] = quest
			QuestLog.info("Adding quest with ID %s" % quest.ID)
			
			self.narrateNewQuest(quest)
			
		else:
			#Message.narrate("You already have the quest, %s" % quest.name)
			Dialogue("You already have the quest, %s" % quest.name)
			
	def markForRemovalByID(self, eID):
		if eID in self.aActiveQuests.keys():
			self.aQuestsMarkedForRemoval.append(eID)
			QuestLog.info_v("Quest with ID %s marked for removal" % eID)
			return True
			
		else:
			QuestLog.warning("Could not mark quest with ID %s for removal" % eID)
			return False
			
	def removeQuestByID(self, eID):
		if eID in self.aActiveQuests.keys():
			del self.aActiveQuests[eID]
			QuestLog.info_v("Removing quest with ID %s" % eID)
			return True
			
		else:
			QuestLog.warning("Could not remove quest with ID %s" % eID)
			return False
				
	def completeQuest(self, quest):
		if self.markForRemovalByID(quest.ID):
			self.aCompletedQuests[quest.ID] = quest
			quest.reward.open(self.parent.team)
			QuestLog.info_v("Completed quest with ID %s" % quest.ID)
		
		else:
			QuestLog.warning("Could not complete quest with ID %s, it's not in aActiveQuests!" % quest.ID)
			
	def removeQuestsThatAreMarkedForRemoval(self):
		for eID in self.aQuestsMarkedForRemoval:
			self.removeQuestByID(eID)
			
		self.aQuestsMarkedForRemoval.clear()
			
	
	def checkQuests(self, context, amount=1):
		QuestLog.info_v("Checking quests with context %s" % context)
		for quest in self.aActiveQuests.values():
			quest.checkContext(context, amount)
			
		self.removeQuestsThatAreMarkedForRemoval()
			
		