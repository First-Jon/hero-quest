
from ID import ID
from Reporter import Reporter

# The base class for all 'things', from items to entities
class Obj(Reporter):
	ID = ID.NONE
	
	def __init__(self):
		self.id = self.__class__.ID