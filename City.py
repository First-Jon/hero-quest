from enum import Enum
import random
import time

from Graphics.ObjPicker import ObjPicker
from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

import Context
from Encounter import Encounter, EncounterReturn
from EntityGroup import EntityGroup
from ID import ID
from Info import InfoType
from Input import Input, InputOption
from Merchant import Merchant
from Message import Message, NarrationType
from Monster import Monster
from Obj import Obj
from Player import Player
	
class SearchState (Enum):
	SEARCHING = 1
	FIGHTING  = 2
	MERCHANT  = 3
	RETURNING = 4
	DEATH     = 5

class City(Obj):
	aRegisteredCities = {}
	aGeneratedCities = {}
	
	ID = ID.GENERIC_CITY
	INFO = False
	INFO_V = False
	DEBUG = False
	WARNING = True
	ERROR = True
	
	def __init__(self, name=""):
		super().__init__()
		self.name = name
		self.aPossibleMonstersWhileSearching = []
		self.aMerchantTypes = []
		self.aActualMerchants = []
		self.player = Player()
		self.enemyTeam  = EntityGroup()
		self.searchState = SearchState.SEARCHING
		
		self.iCostForInn = 20
	
	def enter(self, enteringPlayer):
		self.player = enteringPlayer
		for member in self.player.team.members:
			self.player.questLog.checkQuests(
				Context.Context(Context.Action.ENTER, member.id, ID.ANY, self.id)
			)
		
		self.intro()
		self.chooseAction()
		
	def intro(self):
		#Message.narrate("%s" % self.getStory())
		pass
		
	def getDescription(self):
		super().getDescription()
		
	def chooseAction(self):
		#Message.narrate("%s" % self.getDescription())
		Dialogue("%s" % self.getDescription())
		eUserChoice = Selector(
			{
				InputOption.SEARCH : "'Search' the perimeter!",
				InputOption.SHOP   : "'Shop' in the village square",
				InputOption.REST   : "'Rest' at the village inn",
				InputOption.MANAGE : "'Manage' my party",
				InputOption.CHECK  : "'Check' how many monsters my party has killed",
				InputOption.END    : "'End' I am done for now"
			}
		).openChooseAndClose()
		
		match eUserChoice:
			
			case InputOption.SEARCH:
				self.action_search()
				
			case InputOption.SHOP:
				self.action_shop()
				
			case InputOption.REST:
				self.action_rest()
				
			case InputOption.MANAGE:
				self.action_manage()
				
			case InputOption.CHECK:
				self.action_check()

	def action_search(self):
		self.searchState = SearchState.SEARCHING
		
		while self.searchState !=  SearchState.RETURNING:
		
			match self.searchState:
				
				case SearchState.SEARCHING:

					monsterType = random.choice(self.aPossibleMonstersWhileSearching)
					monsterEncounter = Monster.Generate(monsterType)
			
					self.player.team.print()
			
					#Message.narrate("A %s guards the way, do you wish to attack?" % monsterEncounter.name, NarrationType.NORMAL, 0.1)
					Dialogue("A %s guards the way, do you wish to attack?" % monsterEncounter.name)
			
					eUserChoice = Selector(
						{
							InputOption.ATTACK : "'Attack'",
							InputOption.PASS   : "'Pass'",
							InputOption.RETURN  : "'Return'",
						}
					).openChooseAndClose()
			
					match eUserChoice:
						case InputOption.ATTACK:
							self.enemyTeam.addMember(monsterEncounter)
							self.searchState = SearchState.FIGHTING
						case InputOption.PASS:
							self.searchState = SearchState.SEARCHING
						case InputOption.RETURN:
							self.searchState = SearchState.RETURNING
							
							
							
				case SearchState.FIGHTING:
					encounter = Encounter(self.player.number, self.enemyTeam)
					encounterReturnValue = encounter.executeTillFinished()
						
					self.enemyTeam.clear()
					
					if encounterReturnValue == EncounterReturn.VICTORY:
						self.searchState = SearchState.RETURNING
					elif encounterReturnValue == EncounterReturn.DEFEAT:
						self.searchState = SearchState.DEATH
					else:
						pass
						# Todo, add warning
							
				case SearchState.DEATH:
					time.sleep(3)
					totalKillingBlows = self.player.team.getCombinedInfo(InfoType.KILLING_BLOWS)
					if totalKillingBlows <= 0:
						#Message.narrate("Your team failed to kill a single monster", NarrationType.SLOW, 2.0)
						Dialogue("Your team failed to kill a single monster")
					else:
						#Message.narrate("Monster killed: %d" % totalKillingBlows, NarrationType.SLOW, 2.0)
						Dialogue("Monster killed: %d" % totalKillingBlows)
					#Quest.running = False
					
		self.chooseAction()
		
	def action_shop(self):
		#Message.narrate("Which shop do you want to enter?")
		Dialogue("Which shop do you want to enter?")
		chosenMerchant = ObjPicker(self.aActualMerchants).openChooseAndClose()
		#chosenMerchant = Input.PlayerChoiceFromObjList(self.aActualMerchants)
		chosenMerchant.enterShop(self.player.team)
		
		self.chooseAction()
		
	def action_rest(self):
		for member in self.player.team.members:
			self.player.questLog.checkQuests(
				
				Context.Context(Context.Action.ENTER, member.id, ID.ANY, ID.LOCATION_INN)
			)
		
		#Message.narrate("\nYour party has %d gold" % self.player.team.gold)
		Dialogue("\nYour party has %d gold" % self.player.team.gold)
		
		if self.player.team.checkAffordability(self.iCostForInn):
			#Message.narrate("Rest at the inn? (cost %d)" % self.iCostForInn)
			Dialogue("Rest at the inn? (cost %d)" % self.iCostForInn)
			
			eUserChoice = Selector(
				{
					InputOption.YES : "'Yes' a good rest will do",
					InputOption.NO   : "'No' let's sleep outside"
				}
			).openChooseAndClose()
			
			match eUserChoice:
				case InputOption.YES:
					self.player.team.spendGold(self.iCostForInn)
					for member in self.player.team.members:
						member.heal(bToFull=True, bIncludeMessage=False)
						
					#Message.narrate("Zzz. . . . . . .", NarrationType.VERY_SLOW, 1.0)
					#Message.narrate("The sun rises on a new day with your team fully rejuvinated")
					Dialogue("Zzz. . . . . . .")
					Dialogue("The sun rises on a new day with your team fully rejuvinated")
				case InputOption.NO:
					#Message.narrate("You return to the village streets")
					Dialogue("You return to the village streets")
					pass
					
		else:
			#Message.narrate("Your party doesn't have enough gold for the inn (cost %d)" % self.iCostForInn)
			Dialogue("Your party doesn't have enough gold for the inn (cost %d)" % self.iCostForInn)
			
		self.chooseAction()
		
	def action_manage(self):
		bFinishedManaging = False
		
		while not bFinishedManaging:
			eUserChoice = Selector(
				{
					InputOption.CHECK  : "'Check' party status",
					InputOption.ASSIGN : "'Assign' attribute points",
					InputOption.VIEW   : "'View' inventory",
					InputOption.RETURN : "'Return' to village"
				}
			).openChooseAndClose
			
			match eUserChoice:
				case InputOption.CHECK:
					for member in self.player.team.members:
						member.printStatus()
						
					#Message.narrate("Do you want to check the advanced status of any member?")
					Dialogue("Do you want to check the advanced status of any member?")
					
					eUserChoiceForPartyStatusCheck = Selector(
						{
							InputOption.YES : "'Yes' check advanced status",
							InputOption.NO  : "'No' return to managing party"
						}
					).openChooseAndClose()
					
					match eUserChoiceForPartyStatusCheck:
						case InputOption.YES:
							bDoneCheckingAdvancedStatus = False
							while not bDoneCheckingAdvancedStatus:
								#Message.narrate("Who do you want to check?")
								Dialogue("Who do you want to check?")
								entity = self.player.team.choose()
								if entity:
									entity.printStatus(bIncludeAttributes=True)
								else:
									bDoneCheckingAdvancedStatus = True
						
						case InputOption.NO:
							pass
				
				case InputOption.ASSIGN:
					bReadyToExitAttributeAssignment = False
					while not bReadyToExitAttributeAssignment:
						#Message.narrate("Whose attribute points do you want to assign?")
						Dialogue("Whose attribute points do you want to assign?")
						entity = self.player.team.choose()
						if entity:
							entity.assignAttributePoints()
							
						else:
							bReadyToExitAttributeAssignment = True
				
				case InputOption.VIEW:
					bReadyToExitInventoryView = False
					while not bReadyToExitInventoryView:
						#Message.narrate("Whose inventory do you want to view?")
						Dialogue("Whose inventory do you want to view?")
						entity = self.player.team.choose()
						if entity:
							if entity.inventory.hasItems():
								bReadyToExitItemChoosing = False
								while not bReadyToExitItemChoosing:  # Keep asking the player what item they want to use till they quit
									bReadyToExitItemChoosing = not entity.inventorsy.chooseAndUse() # 'not' is used here since inventory.chooseAndUse return False on failure and True on success
									if not entity.inventory.hasItems(): # After each item used, check if the entity still has items
										#Message.narrate("%s has no more items to use" % entity.name)
										Dialogue("%s has no more items to use" % entity.name)
										bReadyToExitItemChoosing = True
							else:
								#Message.narrate("%s doesn't have any items" % entity.name)
								Dialogue("%s doesn't have any items" % entity.name)
								
						else: # Player decided to exit, quit out of inventory view
							bReadyToExitInventoryView = True
							
				
				case InputOption.RETURN:
					bFinishedManaging = True
					
		
		self.chooseAction()
			
		
	def action_check(self):
		totalKillingBlows = self.player.team.getCombinedInfo(InfoType.KILLING_BLOWS)
		if totalKillingBlows <= 0:
			#Message.narrate("Your team hasn't killed any monsters yet", NarrationType.SLOW)
			Dialogue("Your team hasn't killed any monsters yet")
		else:
			#Message.narrate("Your team has killed %d monsters so far" % totalKillingBlows, NarrationType.SLOW)
			#Message.narrate("Are you pleased with yourself?", NarrationType.SLOW)
			Dialogue("Your team has killed %d monsters so far" % totalKillingBlows)
			Dialogue("Are you pleased with yourself?")
			
		self.chooseAction()
		
	def preGenerateMerchantsForCity(self):
		
		for merchantType in self.aMerchantTypes:
			# Todo, account for invalid merchant type
			self.aActualMerchants.append(Merchant.Generate(merchantType))
			
	@staticmethod
	def Fetch(eID):
		if eID in City.aGeneratedCities:
			return City.aGeneratedCities[eID]
		else:
			pCity = City.Generate(eID)
			
			if pCity == None:
				City.error("Could not fetch city with ID %s, it's not registered!" % eID)
				
			return pCity
	
	@staticmethod
	def Generate(eID):
		pCity = City()
		if eID in City.aRegisteredCities:
			pCity = City.aRegisteredCities[eID]()
			City.aGeneratedCities[eID] = pCity
			
			City.info("Generating city with ID %s" % eID)
			City.info_v("pCity: %s" % pCity)
			
		else:
			City.error("%s is not a registered city ID!" % eID)
			pCity = None

		return pCity
	
	
	@staticmethod
	def Register(pCityClass):
		# Make sure the provided class derives from City
		if City in pCityClass.__bases__:
			City.aRegisteredCities[pCityClass.ID] = pCityClass
			City.info("Registered city with ID %s" % pCityClass.ID)
				
		else:
			City.warning("Failed to register %s! Parent class is not 'City'" % pCityClass)