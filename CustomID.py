'''
	In the future, mods can have their own custom ID.
	
	To do this, they're going to need their own key,
	which will be their identifier for all of their IDs and will help with naming conflicts.
	
	The key itself will grant access to another dictionary where the modder can store their custom IDs.
	
	ex.
	
	CustomID.all[SomeKey][EnumValue]

'''

class CustomID():
	all = {}