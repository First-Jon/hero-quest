from enum import Enum

from Attribute import Attribute, AttributeType
from ItemType.ItemType import ItemTypeAction
from Message import Message
from Reporter import Reporter
from Stat import Stat, StatType

class EquipSlot(Enum):
	NONE = 0
	
	HAND_MAIN   = 10
	HAND_OFF    = 11
	HAND_EITHER = 12
	HAND_BOTH   = 13
	
	HEAD  = 20
	CHEST = 21
	BOOTS = 22
	
	@staticmethod
	def GetString(eEquipSlot):
		returnString = ""
		
		match eEquipSlot:
			case EquipSlot.NONE:
				returnString = "None"
				
			case EquipSlot.HAND_MAIN:
				returnString = "Main hand"
				
			case EquipSlot.HAND_OFF:
				returnString = "Off hand"
				
			case EquipSlot.HAND_EITHER:
				returnString = "Either hand"
			
			case EquipSlot.HAND_BOTH:
				returnString = "Two handed"
				
			case EquipSlot.HEAD:
				returnString = "Head"
				
			case EquipSlot.CHEST:
				returnString = "Chest"
				
			case EquipSlot.BOOTS:
				returnString = "Boots"
				
			case _:
				returnString = ""
				
		return returnString

class Equipment(Reporter):
	INFO = False
	INFO_V = False
	DEBUG = False
	
	def __init__(self, pOwner):
		super().__init__()
		self.mainHand = None
		self.offHand  = None
		
		self.head  = None
		self.chest = None
		self.boots = None
		
		self.all = [
			self.mainHand,
			self.offHand,
			self.head,
			self.chest,
			self.boots
		]
		
		self.items = []
		self.attribute = Attribute()
		self.stat = Stat()
		self.owner = pOwner
		
	# Takes multiple items and adds it to the equipment
	def add(self, *items):
		for item in items:
			item.parent = self
			self.items.append(item)
			
			match item.equipSlot:
				case EquipSlot.HAND_MAIN:
					self.removeItemInSlot(EquipSlot.HAND_MAIN, False)
					item.activeEquipSlot = EquipSlot.HAND_MAIN
					self.mainHand = item
			
				case EquipSlot.HAND_OFF:
					self.removeItemInSlot(EquipSlot.HAND_OFF, False)
						
					# If the main hand is holding a two handed item, make sure to unequip that too
					if self.mainHand == HAND_BOTH:
						self.removeItemInSlot(EquipSlot.HAND_MAIN, False)
						
					item.activeEquipSlot = EquipSlot.HAND_OFF
					self.offHand = item
			
				case EquipSlot.HAND_EITHER:
					pass
				
				case EquipSlot.HAND_BOTH:
					if self.mainHand:
						self.removeItemInSlot(EquipSlot.HAND_MAIN, False)
					if self.offHand:
						self.removeItemInSlot(EquipSlot.HAND_OFF, False)
						
					item.activeEquipSlot = EquipSlot.HAND_MAIN
					self.mainHand = item
					self.offHand = None
				
				case EquipSlot.HEAD:
					self.removeItemInSlot(EquipSlot.HEAD, False)
					item.activeEquipSlot = EquipSlot.HEAD
					self.head = item
						
				case EquipSlot.CHEST:
					self.removeItemInSlot(EquipSlot.CHEST)
					item.activeEquipSlot = EquipSlot.CHEST
					self.chest = item
				
				case EquipSlot.BOOTS:
					self.removeItemInSlot(EquipSlot.BOOTS)
					item.activeEquipSlot = EquipSlot.BOOTS
					self.boots = item
				
				case _:
					self.items.remove(item)
					# Todo, add warning statement

		# Recalculated bonuses after items are added to equipment
		self.recalculate()
		
	def removeItemInSlot(self, eSlot, bRecalculate=True, bDestroy=False):
		pItemToReturnToInventory = None
	
		if eSlot == EquipSlot.HAND_MAIN:
			pItemToReturnToInventory = self.mainHand
			self.mainHand = None
			
		elif eSlot == EquipSlot.HAND_OFF:
			pItemToReturnToInventory = self.offHand
			self.offHand = None

		elif eSlot == EquipSlot.HEAD:
			pItemToReturnToInventory = self.head
			self.head = None
		
		elif eSlot == EquipSlot.CHEST:
			pItemToReturnToInventory = self.chest
			self.chest = None
		
		elif eSlot == EquipSlot.BOOTS:
			pItemToReturnToInventory = self.boots
			self.boots = None
		else:
			pass
			# Todo, add warning statement

		if pItemToReturnToInventory != None:
			pItemToReturnToInventory.parent = None
			pItemToReturnToInventory.activeEquipSlot = EquipSlot.NONE
			self.items.remove(pItemToReturnToInventory)
			
			if not bDestroy:
				#Message.narrate("%s returned to inventory" % pItemToReturnToInventory.name)
				Dialogue("%s returned to inventory" % pItemToReturnToInventory.name)
				
				self.owner.inventory.add(pItemToReturnToInventory)
				
		# Recalculated bonuses after items are removed from equipment
		self.recalculate()
			
	def remove(self, pItem, bDestroy=True):
		if pItem in self.items:
			self.removeItemInSlot(pItem.activeEquipSlot, bDestroy = bDestroy)
		
	def recalculate(self):
		# Reset bonuses
		self.stat.reset()
		self.attribute.reset()
		
		# Re add bonuses
		for item in self.items:
			if item != None:
				self.stat.damage            += item.getStat(StatType.DAMAGE)
				self.stat.armor             += item.getStat(StatType.ARMOR)
				self.stat.firstStrikes      += item.getStat(StatType.FIRSTSTRIKES)
				self.stat.multiStrikeChance += item.getStat(StatType.MULTISTRIKECHANCE)
				self.stat.dodgeChance       += item.getStat(StatType.DODGECHANCE)
				
				self.attribute.agility   += item.getAttribute(AttributeType.AGILITY)
				self.attribute.intellect += item.getAttribute(AttributeType.INTELLECT)
				self.attribute.endurance += item.getAttribute(AttributeType.ENDURANCE)
				self.attribute.strength  += item.getAttribute(AttributeType.STRENGTH)
				
	def getAttribute(self, eAttribute):
		returnValue = self.attribute.getAttribute(eAttribute)
		
		Equipment.info("Equipment, got %s %s for %s" % (returnValue, eAttribute, self.owner))
			
		return returnValue
				
	def getStat(self, eStat, bDamageEquipment=False):
		returnValue = self.stat.getStat(eStat)
		
		if bDamageEquipment:
			if eStat == StatType.ARMOR:
				self.damageArmor()
			
			elif eStat == StatType.DAMAGE:
				self.damageWeapons()
				
		Equipment.info("Equipment, got %s %s for %s" % (returnValue, eStat, self.owner))
		
		return returnValue
				
	def damageArmor(self):
		if self.head != None:
			self.head.lowerDurability()
				
		if self.chest != None:
			self.chest.lowerDurability()
				
		if self.boots != None:
			self.boots.lowerDurability()
		
	def damageWeapons(self):
		if self.mainHand:
			self.mainHand.lowerDurability()
				
		if self.offHand:
			self.offHand.lowerDurability()
			
	def canParry(self):
		bCanParry = False
		
		if self.mainHand:
			if ItemTypeAction.CanParry(self.mainHand.eItemType):
				bCanParry = True
				
		if self.offHand:
			if ItemTypeAction.CanParry(self.offHand.eItemType):
				bCanParry = True
				
		return bCanParry
		
	def canBlock(self):
		bCanBlock = False
		
		if self.mainHand:
			if ItemTypeAction.CanBlock(self.mainHand.eItemType):
				bCanBlock = True
				
		if self.offHand:
			if ItemTypeAction.CanBlock(self.offHand.eItemType):
				bCanBlock = True
				
		return bCanBlock