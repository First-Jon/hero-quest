from Monster import Monster

from ID import ID
from Loot import Loot

class Warg(Monster):
	ID = ID.MONSTER_WARG
	
	def __init__(self):
		super().__init__()
		self.setName("Warg", "the warg")
		self.stat.health_base = 2
		self.attribute.endurance = 4
		self.attribute.strength = 6
		self.stat.armor = 1
		self.gold = 3
		self.attribute.agility = 25
		self.loot = Loot.Generate(ID.LOOT_WARG)
		
				
Monster.Register(Warg)