from Monster import Monster

from ID import ID

class Orc(Monster):
	ID = ID.MONSTER_ORC
	
	def __init__(self):
		super().__init__()
		self.setName("Orc", "the orc")
		self.health_base = 10
		self.attribute.endurance = 10
		self.attribute.strength = 8
		self.stat.armor = 4
		self.gold = 20
				
Monster.Register(Orc)