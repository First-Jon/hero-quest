from Monster import Monster

from ID import ID
from ItemType.Bow import Bow
from Loot import Loot

class Goblin(Monster):
	ID = ID.MONSTER_GOBLIN
	
	def __init__(self):
		super().__init__()
		self.setName("Goblin", "the goblin")
		self.health_base = 10
		self.attribute.intellect = 8
		self.attribute.strength = 5
		self.stat.armor = 2
		self.gold = 10
		self.equipment.add(
			Bow.Generate(ID.ITEM_SIMPLE_SHORTBOW)
		)
		self.loot = Loot.Generate(ID.LOOT_GOBLIN)
		
				
Monster.Register(Goblin)