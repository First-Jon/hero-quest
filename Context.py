
from enum import Enum

from ID import ID
from Reporter import Reporter

class Action(Enum):
	NOTHING  = 1000
	ENTER    = 1001
	TALK     = 1002
	PURCHASE = 1003
	KILL     = 1004
'''	
class Location(Enum):
	ANYWHERE   = 3000
	INN        = 3001
	SHOP       = 3002
	WILDERNESS = 3003
	
class Region(Enum):
	ANYWHERE      = 5000
	ARIGITH_DOMAN = 5001

class Story(Enum):
	INTRO = 2000
	
class Attack(Enum):
	pass
	
class City(Enum):
	INN  = 4000
	SHOP = 4001
'''

class Context(Reporter):
	INFO_V = True
	def __init__(self, eAction=Action.NOTHING, eTargetID=ID.ANY, eConditionalID=ID.ANY, eLocationID=ID.ANY, eRegionID=ID.ANY):
		super().__init__()
		self.eAction        = eAction
		self.eTargetID      = eTargetID
		self.eConditionalID = eConditionalID
		self.eLocationID    = eLocationID
		self.eRegionID      = eRegionID
		
	def matches(self, context):
		# Check if any of the requirements fail to match, otherwise return True
		
		if self.eAction != context.eAction:
			Context.info_v("Action match failed: %s vs %s" % (self.eAction, context.eAction))
			return False
			
		if self.eTargetID != ID.ANY and self.eTargetID != context.eTargetID:
			Context.info_v("Target match failed: %s vs %s" % (self.eTargetID, context.eTargetID))
			return False
			
		if self.eConditionalID != ID.ANY and self.eConditionalID != context.eConditionalID:
			Context.info_v("Conditional match failed: %s vs %s" % (self.eConditionalID, context.eConditionalID))
			return False
			
		if self.eLocationID != ID.ANY and self.eLocationID != context.eLocationID:
			Context.info_v("Location match failed: %s vs %s" % (self.eLocationID, context.eLocationID))
			return False
			
		Context.info_v("Context matched")

		return True
		
	