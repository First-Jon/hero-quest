from enum import Enum

class InfoType(Enum):
	EXPERIENCE = 10
	EXPERIENCE_FOR_NEXT_LEVEL = 11
	CAN_GAIN_EXPERIENCE = 12
	
	LEVEL = 20
	CAN_LEVEL_UP = 21
	
	KILLING_BLOWS = 30
	ATTRIBUTE_POINTS = 40


class Info():
	def __init__(self):
		self.experience = 0
		self.experienceForNextLevel = 50
		self.bCanGainExperience = True
		self.level = 1
		self.bCanLevelUp = False
		self.killingBlows = 0
		self.attributePoints = 0
		
	def getInfo(self, eInfoType):
		returnValue = 0
	
		match eInfoType:
			case InfoType.EXPERIENCE:
				returnValue = self.experience
				
			case InfoType.EXPERIENCE_FOR_NEXT_LEVEL:
				returnValue = self.experienceForNextLevel
				
			case InfoType.CAN_GAIN_EXPERIENCE:
				returnValue = self.bCanGainExperience
				
			case InfoType.LEVEL:
				returnValue = self.level
				
			case InfoType.CAN_LEVEL_UP:
				returnValue = self.bCanLevelUp
				
			case InfoType.KILLING_BLOWS:
				returnValue = self.killingBlows
				
			case ATTRIBUTE_POINTS:
				returnValue = self.attributePoints
				
		return returnValue
		
	def adjust(self, eInfoType, amount):
		match eInfoType:
			case InfoType.EXPERIENCE:
				self.experience += amount
				
			case InfoType.EXPERIENCE_FOR_NEXT_LEVEL:
				self.experienceForNextLevel += amount
				
			case InfoType.CAN_GAIN_EXPERIENCE:
				self.bCanGainExperience = amount
				
			case InfoType.LEVEL:
				self.level += amount
				
			case InfoType.CAN_LEVEL_UP:
				self.bCanLevelUp = amount
				
			case InfoType.KILLING_BLOWS:
				self.killingBlows += amount
				
			case InfoType.ATTRIBUTE_POINTS:
				self.attributePoints += amount
				
	def set(self, eInfoType, value):
		match eInfoType:
			case InfoType.EXPERIENCE:
				self.experience = value
				
			case InfoType.EXPERIENCE_FOR_NEXT_LEVEL:
				self.experienceForNextLevel = value
				
			case InfoType.CAN_GAIN_EXPERIENCE:
				self.bCanGainExperience = value
				
			case InfoType.LEVEL:
				self.level = value
				
			case InfoType.CAN_LEVEL_UP:
				self.bCanLevelUp = value
				
			case InfoType.KILLING_BLOWS:
				self.killingBlows = value
				
			case InfoType.ATTRIBUTE_POINTS:
				self.attributePoints = value
				
	def levelUpAvailable(self):
		if self.bCanLevelUp and self.experience >= self.experienceForNextLevel:
			return True
		else:
			return False
			
	def applyLevelUp(self):
		if self.bCanLevelUp:
			self.level += 1
			self.experience -= self.experienceForNextLevel
			self.experienceForNextLevel = self.level * 50