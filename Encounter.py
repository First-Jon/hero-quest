from enum import Enum
import random
import time

from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

from Input import Input, InputOption
from Message import Message, NarrationType
from Player import Player
from Reporter import Reporter
from Stat import StatType

class EncounterState(Enum):
	INITIATIVE	 = 0
	FIRSTSTRIKES_GOODTEAM = 1
	FIRSTSTRIKE_BADTEAM	  = 2
	FIGHT_GOODTEAM	   = 3
	FIGHT_BADTEAM	   = 4
	CHECKSTATUS	 = 5
	VICTORY		 = 6
	DEFEAT		 = 7
	CLEANUP		 = 8
	
class EncounterReturn(Enum):
	INPROGRESS = 0
	VICTORY	   = 1
	DEFEAT	   = 2
	RUN		   = 3
	
class Team(Enum):
	NONE = 0
	GOOD = 1
	BAD	 = 2
	
class Encounter(Reporter):
	INFO = False
	INFO_V = False
	DEBUG = False
	def __init__(self, ePlayerNumber, badTeam):
		super().__init__()
		#self.goodTeam = goodTeam
		self.ePlayerNumber = ePlayerNumber
		self.badTeam  = badTeam
		
		self.iGoodTeamMembersRunning = 0 # The more good team members running away, the higher chance it'll succeed
		
		self.state = EncounterState.INITIATIVE
		
		self.startingTeam = Team.NONE
		self.encounterReturnValue = EncounterReturn.INPROGRESS
		
	def executeTillFinished(self):
		encounterReturnValue = EncounterReturn.INPROGRESS
					
		while (encounterReturnValue == EncounterReturn.INPROGRESS):
			encounterReturnValue = self.execute()
			
		return encounterReturnValue
		
	def execute(self):
		self.encounterReturnValue = EncounterReturn.INPROGRESS
		
		self.info(self.state)
		
		player = Player.Fetch(self.ePlayerNumber)
		goodTeam = player.team
			
		match self.state:
			case EncounterState.INITIATIVE:
				dRoll = random.randint(1, 100)
				if dRoll <= 50:
					self.startingTeam = Team.GOOD
					self.state = EncounterState.FIRSTSTRIKES_GOODTEAM
					#Message.narrate("Your party has initiative!", NarrationType.FAST, 0.5)
					Dialogue("Your party has initiative!")
				else:
					self.startingTeam = Team.BAD
					self.state = EncounterState.FIRSTSTRIKE_BADTEAM
					#Message.narrate("The enemy has initiative!", NarrationType.FAST, 0.5)
					Dialogue("The enemy has initiative!")
					
				
			case EncounterState.FIRSTSTRIKES_GOODTEAM:
				for member in goodTeam.members:
					for i in range(member.getStat(StatType.FIRSTSTRIKES)):
						if member.playerControlled:
							self.playerAttackChoice(member)
						else:
							self.randomlyAttackIndicatedTeam(member, self.teamBad)
						self.checkEncounterStatus()
						if self.encounterReturnValue != EncounterReturn.INPROGRESS:
							return self.encounterReturnValue
				if self.startingTeam == Team.GOOD:
					self.state = EncounterState.FIRSTSTRIKE_BADTEAM
				else:
					self.state = EncounterState.FIGHT_BADTEAM
					
					
			case EncounterState.FIRSTSTRIKE_BADTEAM:
				for member in self.badTeam.members:
					for i in range(member.getStat(StatType.FIRSTSTRIKES)):
						self.randomlyAttackIndicatedTeam(member, goodTeam)
						self.checkEncounterStatus()
						if self.encounterReturnValue != EncounterReturn.INPROGRESS:
							return self.encounterReturnValue
				if self.startingTeam == Team.BAD:
					self.state = EncounterState.FIRSTSTRIKES_GOODTEAM
				else:
					self.state = EncounterState.FIGHT_GOODTEAM
					

			case EncounterState.FIGHT_GOODTEAM:
				for member in goodTeam.members:
					if member.playerControlled:
						self.playerAttackChoice(member)
					else:
						self.randomlyAttackIndicatedTeam(member, self.badTeam)
						
					self.checkEncounterStatus()
					if self.encounterReturnValue != EncounterReturn.INPROGRESS:
							return self.encounterReturnValue
				self.state = EncounterState.FIGHT_BADTEAM

						
			case EncounterState.FIGHT_BADTEAM:
				for member in self.badTeam.members:
					self.randomlyAttackIndicatedTeam(member, goodTeam)	
					self.checkEncounterStatus()
					if self.encounterReturnValue != EncounterReturn.INPROGRESS:
							return self.encounterReturnValue
				self.state = EncounterState.FIGHT_GOODTEAM

			case _:
				Encounter.warning("WARNING! Encounter.execute had unexpected state: %d" % self.state)
			
		
		if self.iGoodTeamMembersRunning > 0:
			bChanceToRun = (self.iGoodTeamMembersRunning / len(goodTeam.members)) - 0.2

			bRunningSuccessful = random.uniform(0, 1) <= bChanceToRun
			if bRunningSuccessful:
				#Message.narrate("Your team has successfully ran away!")
				Dialogue("Your team has successfully ran away!")
				self.encounterReturnValue = EncounterReturn.RUN
				#self.iGoodTeamMembersRunning = 0
				self.iGoodTeamMembersRunning
				
			else:
				#Message.narrate("Your team has failed to run away!")
				Dialogue("Your team has failed to run away!")
			
		# Just return the current encounterReturnValue
		# This is set in checkEncounterStatus
		return self.encounterReturnValue
		
	def checkEncounterStatus(self):
	
		player = Player.Fetch(self.ePlayerNumber)
		goodTeam = player.team
	
		# Set the encounter return value based on the current status
		if len(self.badTeam.members) <= 0:
			#Message.narrate("You have defeated %s" % self.badTeam.description, dSleepAtEnd = 0.5)
			Dialogue("You have defeated %s" % self.badTeam.description)
			self.encounterReturnValue = EncounterReturn.VICTORY
			
		elif len(goodTeam.members) <= 0:
			#Message.narrate("Your party has been defeated by %s!" % self.badTeam.description, dSleepAtEnd = 0.5)
			Dialogue("Your party has been defeated by %s!" % self.badTeam.description)
			self.encounterReturnValue = EncounterReturn.DEFEAT
		
	def randomlyAttackIndicatedTeam(self, entity, indicatedTeam):
		target = random.choice(indicatedTeam.members)
		
		targetDead = entity.attack(target)
		if targetDead:
			indicatedTeam.removeMember(target)
		
	def playerAttackChoice(self, entity):
	
		#Message.narrate("%s's turn" % entity.name, NarrationType.INSTANT)
		Dialogue("%s's turn" % entity.name)
		entity.printFullStatus(bIncludeName=False)
		print()
		
		actionsLeft = 1
		while actionsLeft > 0:
			
			#eUserChoice = Input.PlayerChoiceFromInputOptions(
			#	{
			#		InputOption.ATTACK	 : "'Attack'",
			#		InputOption.USE		 : "'Use'",
			#		InputOption.ANALYZE	 : "'Analyze'",
			#		InputOption.RUN		 : "'Run'"
			#	}
			#)
			eUserChoice = Selector(
				{
					InputOption.ATTACK	 : "Attack",
					InputOption.USE		 : "Use",
					InputOption.ANALYZE	 : "Analyze",
					InputOption.RUN		 : "Run"
				}
			).openChooseAndClose()

			
			match eUserChoice:
				case InputOption.ATTACK:
					#Message.narrate("Which enemy do you want %s to attack?" % entity.name, NarrationType.FAST, 0.1)
					Dialogue("Which enemy do you want %s to attack?" % entity.name)
					
					target = self.badTeam.choose(bExitAvailable=True)
					if target: # If the target isn't valid, the user decided to exit
						actionsLeft -= 1
						targetDead = entity.attack(target)
						if targetDead:
							self.badTeam.removeMember(target)
					else:
						pass # Execute the while loop again
					
				case InputOption.USE:
					#Message.narrate("What item do you want %s to use?" % entity.name, NarrationType.FAST, 0.1)
					Dialogue("What item do you want %s to use?" % entity.name)
					if entity.inventory.chooseAndUse():
						actionsLeft -= 1
					else:
						pass

				case InputOption.ANALYZE:
					#Message.narrate("Which enemy to do you want %s to analyze?" % entity.name, NarrationType.FAST, 0.1)
					Dialogue("Which enemy to do you want %s to analyze?" % entity.name)
					target = self.badTeam.choose(bExitAvailable=True)
					if target: # If the target isn't valid, the user decided to exit
						actionsLeft -= 1	
						target.analyze()
					else:
						pass

				case InputOption.RUN:
					eUserChoiceForRunning = Selector(
						{
							InputOption.YES : "'Yes' prepare to run away",
							InputOption.NO  : "'No' choose a different action"
						}
					).openChooseAndClose()
					match eUserChoiceForRunning:
						case InputOption.YES:
							#Message.narrate("%s prepares to run away!" % entity.name, NarrationType.FAST, 0.1)
							Dialogue("%s prepares to run away!" % entity.name)
							actionsLeft -= 1
							self.iGoodTeamMembersRunning += 1
						case InputOption.NO:
							pass