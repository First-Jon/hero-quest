from enum import Enum
import random

from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

from Attribute import Attribute, AttributeType
from Equipment import Equipment
from ID import ID
from Info import Info, InfoType
from Inventory import Inventory
from Loot import Loot
from Message import Message, NarrationType
from Obj import Obj
from Stat import Stat, StatType

class EntityID(Enum):
	# Heroes
	FARRON = 1
	JENNI  = 2
	
	# Monsters
	WARG   = 10000
	GOBLIN = 11000
	ORC    = 12000

class Entity(Obj):
	ID = ID.GENERIC_ENTITY
	INFO_V = False
	def __init__(self):
		super().__init__()
		self.name = ""
		self.descriptiveName = ""
		self.strFirstStrike = "fires their bow!" # The text to fire when an entity strikes first
		
		self.stat = Stat()
		
		self.attribute = Attribute(self)
		self.attribute.agility   = 5
		self.attribute.intellect = 5
		self.attribute.endurance = 4
		self.attribute.strength  = 3
		
		self.gold = 0
		
		self.info = Info()
		
		self.playerControlled = False
		self.entityGroup = None

		self.equipment = Equipment(self)
		self.inventory = Inventory(self)
		self.loot = Loot()
		
	def setName(self, name, descriptiveName=""):
		if descriptiveName == "":
			descriptiveName = name
			
		self.name = name
		self.descriptiveName = descriptiveName
		
	def getMaxHealth(self):
		return self.stat.health_base + self.attribute.getStat(StatType.HEALTH_MAX) + self.equipment.getStat(StatType.HEALTH_MAX)
		
	def getAttribute(self, eAttribute):
		return self.attribute.getAttribute(eAttribute) + self.equipment.getAttribute(eAttribute)
		
	def getStat(self, eStat, bDamageEquipment=False):
		return self.stat.getStat(eStat) + self.attribute.getStat(eStat) + self.equipment.getStat(eStat, bDamageEquipment)
		
		
	def attack(self, target):
		chanceForAnotherStrike = self.getStat(StatType.MULTISTRIKECHANCE)
		baseDamage = self.getStat(StatType.DAMAGE, bDamageEquipment=True)
		
		upperDamage = baseDamage * 1.1
		lowerDamage = baseDamage * 0.9
		#Message.narrate("%s attacks %s!" % (self.descriptiveName, target.name))
		Dialogue("%s attacks %s!" % (self.descriptiveName, target.name))
		
		while True:
		
			attackDamage = int(random.uniform(lowerDamage, upperDamage))
			bCriticalHit = random.uniform(0, 1) <= self.getStat(StatType.CRITCHANCE)
			if bCriticalHit:
				attackDamage = self.applyCritical(attackDamage)
			bResult = target.takeDamage(self, attackDamage)
		
			# If the target is dead, don't attempt to strike again
			if bResult == True:
				self.info.adjust(InfoType.KILLING_BLOWS, +1)
				break;
				
			bStrikeAgain = random.uniform(0, 1) <= chanceForAnotherStrike
			if bStrikeAgain:
				#Message.narrate("%s attacks %s again!" % (self.descriptiveName, target.name))
				Dialogue("%s attacks %s again!" % (self.descriptiveName, target.name))
				chanceForAnotherStrike /= 2.0 # Half the strike again chance
			else:
				break; # Failed to strike again, exit
		
		return bResult
		
	def takeDamage(self, attacker, damage):
		bDodgeSuccessful = random.uniform(0, 1) <= self.getStat(StatType.DODGECHANCE)
		if bDodgeSuccessful:
			#Message.narrate("%s dodged the attack!" % self.name)
			Dialogue("%s dodged the attack!" % self.name)
			return False
			
		# Todo, only melee can should be parried,
		# heavier weapons should also be more difficult to parry
		if self.equipment.canParry():
			bParrySuccessful = random.uniform(0, 1) <= self.getStat(StatType.PARRYCHANCE)
			if bParrySuccessful:
				#Message.narrate("%s parried the attack!" % self.name)
				Dialogue("%s parried the attack!" % self.name)
				return False
		
		if self.equipment.canBlock():
			bBlockSuccessful = random.uniform(0, 1) <= self.getStat(StatType.BLOCKCHANCE)
			if bBlockSuccessful:
				#Message.narrate("%s blocked the attack!" % self.name)
				Dialogue("%s blocked the attack!" % self.name)
				return False
		
	
		damageAfterArmor = damage - self.getStat(StatType.ARMOR, bDamageEquipment=True)
		
		if damageAfterArmor < 0:
			damageAfterArmor = 0
			#Message.narrate("The blow glances off %s's armor!" % self.descriptiveName)
			Dialogue("The blow glances off %s's armor!" % self.descriptiveName)
			return False
			
		else:
			self.stat.health_current -= damageAfterArmor
			#Message.narrate("%s takes %s damage!" % (self.descriptiveName, damageAfterArmor))
			Dialogue("%s takes %s damage!" % (self.descriptiveName, damageAfterArmor))
			if self.stat.health_current <= 0:
				self.die(attacker)
				return True
			else:
				return False
				
	def applyCritical(self, damage, bDoMessage=True):
		criticalDamage = damage * (2.0 + self.getStat(StatType.CRITDAMAGEBONUS))
		
		#Message.narrate("Critical hit!", NarrationType.NEAR_INSTANT)
		Dialogue("Critical hit!")
		
		return criticalDamage
				
	def heal(self, amount=0, bToFull=False, bIncludeMessage=True):
		if bToFull:
			self.stat.health_current = self.getMaxHealth()
			if bIncludeMessage and self.playerControlled:
				#Message.narrate("%s healed to full" % self.name)
				Dialogue("%s healed to full" % self.name)
				
		else:
	
			# Adjust the heal amount to perfectly heal to max health
			if self.stat.health_current + amount > self.getMaxHealth():
				amount = self.getMaxHealth() - self.stat.health_current

			self.stat.health_current += amount
			if bIncludeMessage and self.playerControlled:
				#Message.narrate("%s heals %s health" % (self.name, amount))
				Dialogue("%s heals %s health" % (self.name, amount))
		
	def gainExperience(self, experience):
		if self.info.bCanGainExperience:
			self.info.adjust(InfoType.EXPERIENCE, +experience)
			if self.info.levelUpAvailable():
				self.levelUp()
		
	def levelUp(self):
		self.info.applyLevelUp()
		
	def gainItem(self, pItem):
		self.inventory.add(pItem)
		
	def gainItems(self, aItems):
		for item in aItems:
			self.gainItem(item)
		
	def equipItem(self, pItem):
		self.equipment.add(pItem)
		
	def gainGold(self, gold):
		if gold <= 0:
			Entity.info_v("%s recieved %d gold, not proceeding" % gold)
			return
			
		if self.entityGroup != None: # If the entity belongs to a group, give it them instead
			self.entityGroup.gainGold(gold)
		else:
			self.gold += gold
				
	def die(self, attacker):
		#Message.narrate("%s has died!" % self.descriptiveName)
		Dialogue("%s has died!" % self.descriptiveName)
		
		self.loot.open(attacker)
		
	def printStatus(self, bIncludeName=True, bIncludeAttributes=False):
		if (bIncludeName):
			#Message.narrate("%s" % self.name, NarrationType.INSTANT)
			Dialogue("%s" % self.name)
			
		#Message.narrate("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()), NarrationType.INSTANT)
		Dialogue("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()))
		#Message.narrate("  Experience: %d/%d" % (self.info.experience, self.info.experienceForNextLevel), NarrationType.INSTANT) 
		Dialogue("  Experience: %d/%d" % (self.info.experience, self.info.experienceForNextLevel)) 
		#Message.narrate("  Level: %d" % self.info.level, NarrationType.INSTANT) 
		Dialogue("  Level: %d" % self.info.level) 
		
		if bIncludeAttributes:
			#Message.narrate("%s" % self.attribute.getString_all(), NarrationType.INSTANT)
			Dialogue("%s" % self.attribute.getString_all())
		
	def printFullStatus(self, bIncludeName=True):
		if (bIncludeName):
			#Message.narrate("%s" % self.name, NarrationType.INSTANT)
			Dialogue("%s" % self.name)

		#Message.narrate("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()), NarrationType.INSTANT)
		Dialogue("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()))
		#Message.narrate("  Damage: %d" % self.getStat(StatType.DAMAGE), NarrationType.INSTANT)		
		Dialogue("  Damage: %d" % self.getStat(StatType.DAMAGE))		
		#Message.narrate("  Armor: %d" % self.getStat(StatType.ARMOR), NarrationType.INSTANT)
		Dialogue("  Armor: %d" % self.getStat(StatType.ARMOR))
		
		if self.getStat(StatType.MULTISTRIKECHANCE) > 0.0:
			#Message.narrate("  Multistrike Chance: %s" % '{:.1%}'.format(self.getStat(StatType.MULTISTRIKECHANCE)), NarrationType.INSTANT)
			Dialogue("  Multistrike Chance: %s" % '{:.1%}'.format(self.getStat(StatType.MULTISTRIKECHANCE)))
		
		if self.getStat(StatType.FIRSTSTRIKES) > 0:
			#Message.narrate("  First Strikes: %d" % self.getStat(StatType.FIRSTSTRIKES), NarrationType.INSTANT)
			Dialogue("  First Strikes: %d" % self.getStat(StatType.FIRSTSTRIKES))
		#Message.narrate("  Experience: %d/%d" % (self.info.experience, self.info.experienceForNextLevel), NarrationType.INSTANT) 
		Dialogue("  Experience: %d/%d" % (self.info.experience, self.info.experienceForNextLevel)) 
		
	def analyze(self, bIncludeName=True):
		if (bIncludeName):
			#Message.narrate("%s" % self.name, NarrationType.INSTANT)
			Dialogue("%s" % self.name)
		#Message.narrate("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()), NarrationType.INSTANT)
		Dialogue("  Health: %d/%d" % (self.stat.health_current, self.getMaxHealth()))
		#Message.narrate("  Agility: %d" % self.getAttribute(AttributeType.AGILITY), NarrationType.INSTANT)
		Dialogue("  Agility: %d" % self.getAttribute(AttributeType.AGILITY))
		#Message.narrate("  Intellect: %d" % self.getAttribute(AttributeType.INTELLECT), NarrationType.INSTANT)
		Dialogue("  Intellect: %d" % self.getAttribute(AttributeType.INTELLECT))
		#Message.narrate("  Endurance: %d" % self.getAttribute(AttributeType.ENDURANCE), NarrationType.INSTANT)
		Dialogue("  Endurance: %d" % self.getAttribute(AttributeType.ENDURANCE))
		#Message.narrate("  Strength: %d\n" % self.getAttribute(AttributeType.STRENGTH), NarrationType.INSTANT)
		Dialogue("  Strength: %d\n" % self.getAttribute(AttributeType.STRENGTH))
		
		#Message.narrate("  Damage: %d" % self.getStat(StatType.DAMAGE), NarrationType.INSTANT)
		Dialogue("  Damage: %d" % self.getStat(StatType.DAMAGE))
		#Message.narrate("  Armor: %d" % self.getStat(StatType.ARMOR), NarrationType.INSTANT)
		Dialogue("  Armor: %d" % self.getStat(StatType.ARMOR))
		
		if self.getStat(StatType.MULTISTRIKECHANCE) > 0.0:
			#Message.narrate("  Multistrike Chance: %s" % '{:.1%}'.format(self.getStat(StatType.MULTISTRIKECHANCE)), NarrationType.INSTANT)
			Dialogue("  Multistrike Chance: %s" % '{:.1%}'.format(self.getStat(StatType.MULTISTRIKECHANCE)))
		
		if self.getStat(StatType.FIRSTSTRIKES) > 0:
			#Message.narrate("  First Strikes: %d" % self.getStat(StatType.FIRSTSTRIKES), NarrationType.INSTANT)
			Dialogue("  First Strikes: %d" % self.getStat(StatType.FIRSTSTRIKES))
			
		if self.getStat(StatType.CRITCHANCE) > 0:
			#Message.narrate("  Critical Chance: %s" % '{:.1%}'.format(self.getStat(StatType.CRITCHANCE)), NarrationType.INSTANT)
			Dialogue("  Critical Chance: %s" % '{:.1%}'.format(self.getStat(StatType.CRITCHANCE)))
			
		if self.getStat(StatType.CRITDAMAGEBONUS) > 0:
			#Message.narrate("  Critical Damage Bonus: %s" % '{:.1%}'.format(self.getStat(StatType.CRITDAMAGEBONUS)), NarrationType.INSTANT)
			Dialogue("  Critical Damage Bonus: %s" % '{:.1%}'.format(self.getStat(StatType.CRITDAMAGEBONUS)))
		