from Loot import Loot
from ID import ID

class Quest_JennisStory(Loot):
	ID = ID.REWARD_JENNI_S_STORY
	
	def __init__(self):
		super().__init__()
		self.experience_low = 40
		self.experience_high = 40
		
		self.gold_low = 20
		self.gold_high = 20
				
Loot.Register(Quest_JennisStory)