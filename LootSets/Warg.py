from Loot import Loot

from ID import ID

class Warg(Loot):
	ID = ID.LOOT_WARG
	
	def __init__(self):
		super().__init__()
		
		self.experience_low = 5
		self.experience_high = 10
				
Loot.Register(Warg)