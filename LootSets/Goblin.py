from Loot import Loot, LootItem
from ID import ID
from ItemType.Consumable import Consumable

class Goblin(Loot):
	ID = ID.LOOT_GOBLIN
	
	def __init__(self):
		super().__init__()
		
		self.experience_low = 8
		self.experience_high = 16
		
		self.gold_low = 3
		self.gold_high = 11
		
		self.aPossibleItems = [
			LootItem(
				Consumable.Generate(ID.ITEM_WEAK_HEALING_POTION),
				chance=0.9
			)
		]
				
Loot.Register(Goblin)