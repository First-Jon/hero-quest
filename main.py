import ImportOnce
from time import sleep

from ID import ID
from Player import Player, PlayerNumber
from Story import Story
from StoryThread import StoryThread

from Graphics.Graphics import Graphics
from Graphics.Menu import Menu
from Graphics.Screen import Screen

from pygame import event

def main():
	Graphics.Initialize()
	StoryThread.Initialize()
	
	#Player.Fetch(PlayerNumber.ONE).addMenu(Menu.Generate(ID.MENU_MAIN))
	#Player.Release(PlayerNumber.ONE)
	Menu.Generate(ID.MENU_MAIN).open()
	
	while True:
		sleep(0.5)
		event.pump()

	#playerOne.setActiveMenu(ID.MENU_MAIN)
	
	#introStory = Story.Generate(ID.STORY_INTRO)
	#introStory.start(playerOne)
	
	

if __name__ == "__main__":
	main()