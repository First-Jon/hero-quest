
import Cursor
import time
from enum import Enum
import keyboard

class NarrationType(Enum):
	VERY_SLOW = 1
	SLOW = 2
	NORMAL = 3
	FAST = 4
	VERY_FAST = 5
	NEAR_INSTANT = 6
	INSTANT = 7

class Message():
	bInfoEnabled     = True
	bVerboseInfoEnabled = True
	bWarningsEnabled = True
	bErrorsEnabled   = True
	bDebugEnabled    = False
	bInstantNarration = True
	
	aNarrationSpeed = {
		NarrationType.VERY_SLOW : 0.1,
		NarrationType.SLOW : 0.05,
		NarrationType.NORMAL : 0.02,
		NarrationType.FAST : 0.008,
		NarrationType.VERY_FAST : 0.004,
		NarrationType.NEAR_INSTANT : 0.001,
		NarrationType.INSTANT : 0.0
	}
	
	strFastForwardKey = 'shift'
	
	@staticmethod
	def getNarrationSpeed(eNarrationType):
		if Message.bInstantNarration:
			return Message.aNarrationSpeed[NarrationType.INSTANT]
	
		if eNarrationType in Message.aNarrationSpeed:
			return Message.aNarrationSpeed[eNarrationType]
		else:
			Message.error("%s is not a valid narration speed!" % eNarrationType)
			return Message.aNarrationSpeed[NarrationType.INSTANT]

	@staticmethod
	def narrate(string, eNarrationType = NarrationType.NORMAL, dSleepAtEnd = 0.0, bNewLine=True):
		Cursor.hide_cursor()
		dNarrationSpeed = Message.getNarrationSpeed(eNarrationType)
		
		# If the narration speed is instant, just print out the string
		if dNarrationSpeed <= 0.0:
			print(string, end='', flush=True)
		else:
			for character in string:
				print(character, end='', flush=True)
				if keyboard.is_pressed(Message.strFastForwardKey):
					time.sleep(Message.getNarrationSpeed(NarrationType.NEAR_INSTANT))
				else:
					time.sleep(dNarrationSpeed)
		
		if bNewLine:
			print()
			
		time.sleep(dSleepAtEnd)
		
	@staticmethod
	def info(string):
		if Message.bInfoEnabled:
			print("INFO %s" % string)
			
	@staticmethod
	def info_v(string):
		if Message.bVerboseInfoEnabled:
			print("INFO_V %s" % string)
		
	@staticmethod
	def warning(string):
		if Message.bWarningsEnabled:
			print("WARN %s" % string)
			
	@staticmethod
	def error(string):
		if Message.bErrorsEnabled:
			print("ERROR %s" % string)
			
	@staticmethod
	def debug(string):
		if Message.bDebugEnabled:
			print("DEBUG %s" % string)