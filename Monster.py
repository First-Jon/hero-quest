from enum import Enum

from Entity import Entity
from ID import ID
from Item import Item
from Loot import Loot
	
class Monster(Entity):
	aRegisteredMonsters = {}
	ID = ID.GENERIC_MONSTER
	
	def __init__(self):
		super().__init__()
		
	@staticmethod
	def Generate(eID):
		pMonster = Monster()
		if eID in Monster.aRegisteredMonsters:
			pMonster = Monster.aRegisteredMonsters[eID]()
			Monster.info("Generating monster with ID %s" % eID)
			Monster.info_v("pMonster: %s" % pMonster)
		else:
			Monster.error("%s is not a registered monster ID!" % eID)

		pMonster.heal(bToFull=True)
		return pMonster
		
	@staticmethod
	def Register(pMonsterClass):
		# Make sure the provided class derives from Monster
		if Monster in pMonsterClass.__bases__:
			Monster.aRegisteredMonsters[pMonsterClass.ID] = pMonsterClass
			Monster.info("Registered monster with ID %s" % pMonsterClass.ID)
				
		else:
			Monster.warning("Failed to register %s! Parent class is not 'Monster'" % pMonsterClass)
		

#Warg = Monster('Warg', 14, 4, 8, 0, 3, 0, .25)
#Goblin = Monster('Goblin', 16, 5, 5, 0, 10, 1)
#Orc = Monster('Orc', 20, 10, 8, 1, 20)
#UrukHai = Monster('Uruk Hai', 30, 15, 11, 2, 30)
#Troll = Monster('Troll', 40, 25, 14, 3, 40)
#Ogre = Monster('Ogre', 70, 30, 20, 0, 50) 