from enum import Enum

from Reporter import Reporter

class StatType(Enum):
	KILLINGBLOWS = 10
	EXPERIENCE  = 20
	LEVEL = 30
	
	HEALTH_BASE     = 90 # The natural amount of health an entity has
	HEALTH_CURRENT  = 91
	HEALTH_MAX      = 92 # An entity's maxiumum health, usually calculated using the base health, attributes, and equipment.
	DAMAGE = 100
	ARMOR  = 110
	FIRSTSTRIKES = 120
	MULTISTRIKECHANCE = 130
	DODGECHANCE = 140
	HEALING = 150
	CRITCHANCE = 160
	CRITDAMAGEBONUS = 170
	PARRYCHANCE = 180
	BLOCKCHANCE = 190
	
	
class Stat(Reporter):
	INFO = False
	def __init__(self, health_base=0, health_current=0, health_max=0, damage=0, armor=0, firstStrikes=0, multiStrikeChance=0, dodgeChance=0, healing=0, critChance=0, critDamageBonus=0, parryChance=0, blockChance=0):
		super().__init__()
		self.health_base = health_base
		self.health_current = health_current
		self.health_max = health_max
		self.damage = damage
		self.armor = armor
		self.firstStrikes = firstStrikes
		self.multiStrikeChance = multiStrikeChance
		self.dodgeChance = dodgeChance
		self.healing = healing
		self.critChance = critChance
		self.critDamageBonus = critDamageBonus
		self.parryChance = parryChance
		self.blockChance = blockChance
		
	def getStat(self, eStat):
		returnValue = 0
		match eStat:
			case StatType.HEALTH_BASE:
				returnValue = self.health_base
				
			case StatType.HEALTH_CURRENT:
				returnValue = self.health_current
				
			case StatType.HEALTH_MAX:
				returnValue = self.health_max
				
			case StatType.DAMAGE:
				returnValue = self.damage
			
			case StatType.ARMOR:
				returnValue = self.armor
			
			case StatType.FIRSTSTRIKES:
				returnValue = self.firstStrikes
			
			case StatType.MULTISTRIKECHANCE:
				returnValue = self.multiStrikeChance
			
			case StatType.DODGECHANCE:
				returnValue = self.dodgeChance
				
			case StatType.HEALING:
				returnValue = self.healing
				
			case StatType.CRITCHANCE:
				returnValue = self.critChance
				
			case StatType.CRITDAMAGEBONUS:
				returnValue = self.critDamageBonus
				
			case StatType.PARRYCHANCE:
				returnValue = self.parryChance
				
			case StatType.BLOCKCHANCE:
				returnValue = self.blockChance
			
			case _:
				pass
				# todo, add warning
				
		Stat.info("Stat, got %s %s" % (returnValue, eStat))
			
		return returnValue
		
	def reset(self):
		self.health_base = 0
		self.health_current = 0
		self.health_max = 0
		self.damage = 0
		self.armor = 0
		self.firstStrikes = 0
		self.multiStrikeChance = 0
		self.dodgeChance = 0
		self.healing = 0
		self.critChance = 0
		self.critDamageBonus = 0
		self.parryChance = 0
		self.blockChance = 0