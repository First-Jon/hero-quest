from enum import Enum

# Unlike attributes, virtues have to earned through special quests and feats of strength.
# Not only do they provide bonuses in combat and other situations
# But they may also unlock new opportunities

class VirtueType(Enum):
'''
  Reduces enemy crit chance
'''
	WISDOM = 10
	
'''

'''
	COURAGE = 20