from enum import Enum

from Graphics.TextTypes.Dialogue import Dialogue

from Attribute import Attribute
from Equipment import EquipSlot
from ID import ID
from ItemType.ItemType import ItemType
from Message import Message
from Obj import Obj
from Stat import Stat, StatType

'''
class PredefinedItem(Enum):
	SWORD = 100
	SWORD_SIMPLE_IRON_BLADE = 101
	
	DAGGER = 200
	DAGGER_SIMPLE = 201
	
	BOW = 300
	BOW_SIMPLE_SHORTBOW = 301
	
	CONSUMABLE = 400
	CONSUMABLE_WEAK_HEALING_POTION = 410
	CONSUMABLE_MODERATE_HEALING_POTION = 411
	
	HELMET = 500
	
	CHEST = 600
	CHEST_WEATHERED_LEATHER_TUNIC = 601
	
	BOOTS = 700
	BOOTS_WEATHERED_LEATHER_BOOTS = 701
	
	STAFF = 800
	STAFF_WALKING_STICK = 801
'''
	
class ConsumableType(Enum):
	NONE  = 0
	DRINK = 10
	THROW = 20
	
class Item(Obj):
	ID = ID.GENERIC_ITEM
	def __init__(self, name = "", equipSlot = EquipSlot.NONE, durability = 0, consumable = -1, consumableType = ConsumableType.NONE):
		super().__init__()
		self.name = name
		self.equipSlot = equipSlot # The equipment slot an item is intended for
		self.activeEquipSlot = EquipSlot.NONE # The equipment slot an item is currently in
		
		self.eItemType = ItemType.NONE
		
		self.stat = Stat()
		self.attribute = Attribute()

		self.consumable = consumable
		self.consumableType = consumableType
		
		self.durability = durability # How long an object lasts, for consumables this appears as "uses"
		
		self.parent = None
		
	def use(self, entity):
		if self.consumableType == ConsumableType.NONE:
			entity.equipItem(self)
		else:
			if self.getStat(StatType.HEALING):
				entity.heal(self.getStat(StatType.HEALING))
				
			self.lowerDurability()
			
			
	def lowerDurability(self):
		self.durability -= 1
		self.checkDurability()
			
	def checkDurability(self):
		if self.durability <= 0:
			if self.consumableType == ConsumableType.NONE:
				#Message.narrate("The %s broke!" % self.name)
				Dialogue("The %s broke!" % self.name)
			else:
				#Message.narrate("The %s has been used up!" % self.name)
				Dialogue("The %s has been used up!" % self.name)
				
			# Remove the item from the parent's container
			self.parent.remove(self, bDestroy = True)
			
	def getAttribute(self, eAttribute):
		return self.attribute.getAttribute(eAttribute)
			
	def getStat(self, eStat):
		return self.stat.getStat(eStat) + self.attribute.getStat(eStat)
			
	def getDescription(self, bIncludeName = True):
		returnString = ""
		
		if bIncludeName:
			returnString += self.name + "\n"
		
		equipSlotString = EquipSlot.GetString(self.equipSlot)
		if equipSlotString != "" and equipSlotString != "None":
			returnString += "\t%s\n" % equipSlotString
		
		if self.stat.damage != 0:
			returnString += "\tDamage: %d\n" % self.stat.damage
			
		if self.stat.armor != 0:
			returnString += "\tArmor: %d\n" % self.stat.armor
			
		if self.stat.firstStrikes != 0:
			returnString += "\tFirst Strikes: %d\n" % self.stat.firstStrikes
			
		if self.stat.multiStrikeChance != 0.0:
			returnString += "\tMultistrike Chance: %f\n" % self.stat.multiStrikeChance
			
		if self.stat.healing != 0:
			returnString += "\tHealing: %d\n" % self.stat.healing
			
		if self.attribute.agility != 0:
			returnString += "\tAgility: %d\n" % self.attribute.agility
			
		if self.attribute.intellect != 0:
			returnString += "\tIntellect: %d\n" % self.attribute.intellect
			
		if self.attribute.endurance != 0:
			returnString += "\Endurance: %d\n" % self.attribute.endurance
			
		if self.attribute.strength != 0:
			returnString += "\tAgility: %d\n" % self.attribute.strength

		if self.consumableType == ConsumableType.NONE:
			returnString += "\tDurability: %d" % self.durability
		else:
			returnString += "\tUses: %d" % self.durability
			
		return returnString