import threading

from Story  import Story

from Player import Player, PlayerNumber

class StoryThread(threading.Thread):
	this = None
	def __init__(self):
		if StoryThread.this != None:
			raise Exception("Only one instance of StoryThread can be initialized!")
		else:
			super().__init__()
			self.shutdown_flag = threading.Event()
			
			self.story  = None
			self.bStoryIsSet = threading.Event()
			
			self.ePrimaryPlayerNumber = PlayerNumber.ONE
		
	def run(self):
		while not self.shutdown_flag.is_set():
			self.bStoryIsSet.wait()
			if self.story:
				self.story.start()
			else:
				# Todo, add warning, bStoryIsSet even though self.story is none
				pass
			self.bStoryIsSet.clear()
			
	def setStory(self, storyID):
		self.story = Story.Generate(storyID)
		self.bStoryIsSet.set()
		
	def setPrimaryPlayerNumber(self, ePlayerNumber):
		self.ePrimaryPlayerNumber = ePlayerNumber
		
	def getPrimaryPlayer(self):
		return Player.Fetch(self.ePrimaryPlayerNumber)
		
	@staticmethod
	def Get():
		return StoryThread.this
		
	@staticmethod
	def GetPlayer():
		return StoryThread.this.getPrimaryPlayer()
		
	@staticmethod
	def Initialize():
		StoryThread.this = StoryThread()
		StoryThread.this.start()