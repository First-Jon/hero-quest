from Hero import Hero

from ID import ID

class Farron(Hero):
	ID = ID.HERO_FARRON
	
	def __init__(self):
		super().__init__()
		self.setName("Farron")
		self.stat.health_base = 15
		self.attribute.agility = 4
		self.attribute.intellect = 5
		self.attribute.endurance = 6
		self.attribute.strength = 7
		self.gold = 10
		
				
Hero.Register(Farron)