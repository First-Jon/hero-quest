from Hero import Hero

from ID import ID

class Jenni(Hero):
	ID = ID.HERO_JENNI
	
	def __init__(self):
		super().__init__()
		self.setName("Jenni")
		self.stat.health_base = 5
		self.attribute.agility = 6
		self.attribute.intellect = 9
		self.attribute.endurance = 7
		self.attribute.strength = 2
		self.playerControlled = False
		self.gold = 120
		
				
Hero.Register(Jenni)