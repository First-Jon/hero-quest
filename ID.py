from enum import Enum, auto

class ID(Enum):
	NONE = auto(),
	ANY  = auto(),
	
# Generic IDs
	LOCATION_INN = auto(),
	GENERIC_CITY = auto(),
	GENERIC_ENTITY = auto(),
	GENERIC_HERO = auto(),
	GENERIC_ITEM = auto(),
	GENERIC_LOOT = auto(),
	GENERIC_MERCHANT = auto(),
	GENERIC_MONSTER = auto(),
	GENERIC_QUEST = auto(),
	GENERIC_STORY = auto(),
	
	GENERIC_BOOT = auto(),
	GENERIC_BOW = auto(),
	GENERIC_CHEST = auto(),
	GENERIC_CONSUMABLE = auto(),
	GENERIC_DAGGER = auto(),
	GENERIC_HELMET = auto(),
	GENERIC_STAFF = auto(),
	GENERIC_SWORD = auto(),
	
	GENERIC_MENU = auto(),
	GENERIC_SPRITE = auto(),
	GENERIC_TEXT = auto(),
	GENERIC_UI = auto(),
	
# Heroes
	HERO_FARRON = auto(),
	HERO_JENNI = auto(),
	
# Monsters
	MONSTER_WARG = auto(),
	MONSTER_GOBLIN = auto(),
	MONSTER_ORC = auto(),
	
# Merchants
	MERCHANT_JERRILS_HERBS_AND_MORE = auto(),
	MERCHANT_NICKS_STICKS_AND_THINGS = auto(),
	
# Cities
	CITY_ARIGITH_DOMAN = auto(),
	
# Items
	
	# Swords
	ITEM_SIMPLE_IRON_BLADE = auto(),
	
	# Daggers
	ITEM_DAGGER_SIMPLE = auto(),
	
	# Staffs
	ITEM_WALKING_STICK = auto(),
	
	# Bows
	ITEM_SIMPLE_SHORTBOW = auto(),
	
	# Consumables
	ITEM_WEAK_HEALING_POTION = auto(),
	ITEM_MODERATE_HEALING_POTION = auto(),
	
	# Helmets
	
	# Chests
	ITEM_WEATHERED_LEATHER_TUNIC = auto(),
	
	
	# Boots
	ITEM_WEATHERED_LEATHER_BOOTS = auto(),
	
# Loot
	LOOT_WARG = auto(),
	LOOT_GOBLIN = auto(),
	REWARD_JENNI_S_STORY = auto(),
	
	
# Quests
	QUEST_JENNI_S_STORY = auto(),

# Stories
	STORY_INTRO = auto(),
	
# Menus
	MENU_MAIN = auto(),
	MENU_OPTIONS = auto(),
	MENU_SELECTOR = auto(),
	MENU_OBJ_PICKER = auto(),
	
# Text Types
	TEXTTYPE_DIALOGUE = auto(),