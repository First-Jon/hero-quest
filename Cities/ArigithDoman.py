from City import City

import Context
from ID import ID
from Merchant import Merchant
from Message import Message

class ArigithDoman(City):
	ID = ID.CITY_ARIGITH_DOMAN
	
	def __init__(self):
		super().__init__()
		self.name = "Arigith Doman"
		
		self.aPossibleMonstersWhileSearching = [
			ID.MONSTER_WARG,
			ID.MONSTER_GOBLIN
		]
		
		self.aMerchantTypes = [
			ID.MERCHANT_JERRILS_HERBS_AND_MORE,
			ID.MERCHANT_NICKS_STICKS_AND_THINGS
		]
		
		self.preGenerateMerchantsForCity()
		
	def enter(self, enteringPlayerTeam):
		super().enter(enteringPlayerTeam)
		
	def intro(self):
		pass
		#if context == Context.Story.INTRO:
		#	
		
	def getDescription(self):
		return ("Arigith Doman - The Crossroads")
	

	def getStory(self):
		return ("Arigith Doman was a prosperous town, it sat between the great cities,\n"
				"and all manner of people through on their way to see one or another.\n"
				"As the cities expanded, so did Arigith.\n"
				"Soon, it was called by many, as the Crossroads.\n"
	
				"However, too slowly to be seen and too fast to be stopped,\n"
				"the great cities fell into decay.\n"
				"Corruption took one,\n"
				"fear took another,\n"
				"and plain malice drove one of the great kings mad.\n"
		
				"The crossroads was forgotten, and with that so was Arigith.\n"
				"No longer did caravans come through the town,\n"
				"and in isolation is has shrunk.\n"
		
				"That was.. till the year of the golden moon, the year invaders took the land.\n"
				"In desperate flight refugees poured across the earth,\n"
				"and a few remembered the old trails to the crossroads.\n")
				
				
City.Register(ArigithDoman)