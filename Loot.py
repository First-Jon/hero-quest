from enum import Enum
import random

from ID import ID
from Item import Item
from Obj import Obj


class LootItem():
	def __init__(self, item=Item(), chance=1.0):
		self.item = item
		self.name = self.item.name + " loot"
		self.chance = chance

class Loot(Obj):
	aRegisteredLoot = {}
	
	ID = ID.GENERIC_LOOT
	INFO = False
	INFO_V = False
	WARNING = True
	ERROR = True

	def __init__(self):
		super().__init__()
		
		self.gold_low = 0
		self.gold_high = 0
		
		self.experience_low = 0
		self.experience_high = 0
		
		self.aPossibleItems = []
		
	def open(self, pEntityOrGroup):
		
		actualGold = self.determineGold()
		actualExperience = self.determineExperience()
		actualItems = self.determineItems()
		
		if actualGold > 0:
			pEntityOrGroup.gainGold(actualGold)
		if actualExperience > 0:
			pEntityOrGroup.gainExperience(actualExperience)
		if len(actualItems) > 0:
			pEntityOrGroup.gainItems(actualItems)
		
	
		
	def determineGold(self):
		return random.randint(self.gold_low, self.gold_high)
		
	def determineExperience(self):
		return random.randint(self.experience_low, self.experience_high)
		
	def determineItems(self):
		actualItems = []
		for lootItem in self.aPossibleItems:
			if random.uniform(0, 1) <= lootItem.chance:
				actualItems.append(lootItem.item)
				
		return actualItems
		
		
		
	@staticmethod
	def Generate(eID):
		pLoot = Loot()
		if eID in Loot.aRegisteredLoot:
			pLoot = Loot.aRegisteredLoot[eID]()
			Loot.info("Generating loot with ID %s" % eID)
			Loot.info_v("pLoot: %s" % pLoot)
		else:
			Loot.error("%s is not a registered loot ID!" % eID)
				
		return pLoot
		
	@staticmethod
	def Register(pLootClass):
		# Make sure the provided class derives from Loot
		if Loot in pLootClass.__bases__:
			Loot.aRegisteredLoot[pLootClass.ID] = pLootClass
			Loot.info("Registered loot with ID %s" % pLootClass.ID)
				
		else:
			Loot.warning("Failed to register %s! Parent class is not 'Loot'" % pLootClass)