
from enum import Enum
import random

from Graphics.ObjPicker import ObjPicker
from Graphics.TextTypes.Dialogue import Dialogue
from Graphics.Selector import Selector

from ID import ID
from Input import Input, InputOption
from Item import Item
from ItemType.Bow import Bow
from ItemType.Consumable import Consumable
from ItemType.Staff import Staff
from Message import Message, NarrationType
from Obj import Obj

class MerchantItem():
	def __init__(self, item=Item(), cost=5, amount=1):
		self.item = item
		self.name = self.item.name
		self.cost = cost
		self.amount = amount
		
	def getDescription(self):
		returnString = ""

		if self.amount > 1:
			returnString += "(cost for %d, %d)\n" % (self.amount, self.cost)
		else:
			returnString += "(cost %d)\n" % self.cost
				
			returnString += self.item.getDescription(bIncludeName=False)
			
		return returnString
'''	
class MerchantShop():
	# Threat Level 1
	JERRILS_HERBS_AND_MORE = 10
	NICKS_STICKS_AND_THINGS = 11
	
	# Threat Level 2
	THE_BRANDISHED_BOW = 20
	
	# Threat Level 3
	FORGOTTEN_ELIXERS = 30
	
	# Threat Level 4
	THE_ANCIENT_BLADE = 40
'''


class Merchant(Obj):
	# aMerchantShopsOrganizedByThreatLevel = [
		# [ # Threat Level 1
			# MerchantShop.JERRILS_HERBS_AND_MORE,
			# MerchantShop.NICKS_STICKS_AND_THINGS
		# ],
		
		# [ # Threat Level 2
			# MerchantShop.THE_BRANDISHED_BOW
		# ],
		
		# [ # Threat Level 3
			# MerchantShop.FORGOTTEN_ELIXERS
		# ],
		
		# [ # Threat Level 4
			# MerchantShop.THE_ANCIENT_BLADE
		# ],
	
	# ]
	
	aRegisteredMerchants = {}
	ID = ID.GENERIC_MERCHANT

	def __init__(self, name = "", introduction = "You find a merchant along the way"):
		super().__init__()
		self.name = name
		self.introduction = introduction
		self.aMerchantItemsToSell = []
		
	@staticmethod
	def FetchMerchantBasedOnThreatLevel(iThreatLevel):
		aMerchantsForThreatLevel = Merchant.aMerchantShopsOrganizedByThreatLevel[iThreatLevel - 1] # The threat level starts at 1, so this is neccessary
		eChosenMerchant = random.choice(aMerchantsForThreatLevel)		
		return eChosenMerchant
		
	@staticmethod
	def GenerateBasedOnThreatLevel(iThreatLevel):
		eChosenMerchant = Merchant.FetchMerchantBasedOnThreatLevel(iThreatLevel)
		return Merchant.Generate(eChosenMerchant)

	def chooseItem(self, entityGroup):
		if len(self.aMerchantItemsToSell) <= 0:
			#Message.narrate("%s has no more items left to sell." % self.name)
			Dialogue("%s has no more items left to sell." % self.name)
			return False

		#Message.narrate("\nYour party has %d gold" % entityGroup.gold)
		Dialogue("\nYour party has %d gold" % entityGroup.gold)
		#Message.narrate("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=", NarrationType.NEAR_INSTANT)
		
		#chosenMerchantItem, index = Input.PlayerChoiceFromObjList(self.aMerchantItemsToSell, bIncludeDescriptions=True, bReturnIndex=True, bExitAvailable=True)
		chosenMerchantItem, index = ObjPicker(self.aMerchantItemsToSell, bIncludeDescriptions=True, bReturnIndex=True, bExitAvailable=True).openChooseAndClose()
		
		# The player chose to exit out of the shop
		if chosenMerchantItem == False:
			#Message.narrate("You exit the shop")
			Dialogue("You exit the shop")
			return False
		
		# Price check
		if entityGroup.checkAffordability(chosenMerchantItem.cost):
			#Message.narrate("Purchase %s for %d? (your party has %d gold)" % (chosenMerchantItem.name, chosenMerchantItem.cost, entityGroup.gold))
			Dialogue("Purchase %s for %d? (your party has %d gold)" % (chosenMerchantItem.name, chosenMerchantItem.cost, entityGroup.gold))
			
			eUserChoice = Selector(
				{
					InputOption.YES    : "'Yes' purchase item",
					InputOption.CHANGE : "'Change' I want to pick something else",
					InputOption.LEAVE  : "'Leave' the shop",
				}
			).openChooseAndClose()
			
			match eUserChoice:
				case InputOption.YES:
					entityGroup.spendGold(chosenMerchantItem.cost)
					del self.aMerchantItemsToSell[index]
					entityGroup.gainItem(chosenMerchantItem.item)
				case InputOption.CHANGE:
					# This will list the items again
					return True
				
				case InputOption.LEAVE:
					#Message.narrate("You exit the shop")
					Dialogue("You exit the shop")
					return False
			
			
			
		else:
			#Message.narrate("What are you trying to pass? You don't have enough gold")
			Dialogue("What are you trying to pass? You don't have enough gold")

		return True
			
	def enterShop(self, entityGroup):
		#Message.narrate("%s" % self.introduction)
		Dialogue("%s" % self.introduction)
		
		while self.chooseItem(entityGroup):
			pass
			
		
	@staticmethod
	def Generate(eID):
		pMerchant = Merchant()
		if eID in Merchant.aRegisteredMerchants:
			pMerchant = Merchant.aRegisteredMerchants[eID]()
			Merchant.info("Generating merchant with ID %s" % eID)
			Merchant.info_v("pMerchant: %s" % pMerchant)
		else:
			Merchant.error("%s is not a registered merchant ID!" % eID)

		return pMerchant
		
	@staticmethod
	def Register(pMerchantClass):
		# Make sure the provided class derives from Merchant
		if Merchant in pMerchantClass.__bases__:
			Merchant.aRegisteredMerchants[pMerchantClass.ID] = pMerchantClass
			Merchant.info("Registered merchant with ID %s" % pMerchantClass.ID)
				
		else:
			Merchant.warning("Failed to register %s! Parent class is not 'Merchant'" % pMerchantClass)