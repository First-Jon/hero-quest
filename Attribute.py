from enum import Enum

from Message import Message
from Reporter import Reporter
from Stat import StatType
	
class AttrAgility():
	DodgeBonus = 0.01
	MultiStrikeBonus = 0.01
	
class AttrIntellect():
	CritBonus = 0.01
	
class AttrEndurance():
	HealthBonus = 2
	BlockBonus = 0.01
	
class AttrStrength():
	DamageBonus = 1
	CritDamageBonus = 0.02
	ParryBonus = 0.01
	
class AttributeType(Enum):

# Improves dodge chance
# Improves multi-strike chance
# Improves counter-strike chance
	AGILITY = 10
	
# Improves critical chance
# Improves party's initiative
	INTELLECT = 20

# Improves run chance
# Improves health
# Improves block chance
# Reduces enemy run chance
# Reduces multi-strike decay
	ENDURANCE = 30
	
# Improves damage
# Improves parry chance
# Reduces enemey parry chance
# Improves critical damage
	STRENGTH = 40
	
	@staticmethod
	def GetDescription(eAttribute):
		returnString = ""
		match eAttribute:
			case AttributeType.AGILITY:
				returnString += "Agility\n"
				returnString += "  +%s dodge chance\n" % '{:.0%}'.format(AttrAgility.DodgeBonus)
				returnString += "  +%s multi-strike chance\n" % '{:.0%}'.format(AttrAgility.MultiStrikeBonus)
				
			case AttributeType.INTELLECT:
				returnString += "Intellect\n"
				returnString += "  +%s critical chance\n" % '{:.0%}'.format(AttrIntellect.CritBonus)
				
				
			case AttributeType.ENDURANCE:
				returnString += "Endurance\n"
				returnString += "  +%d health\n" % AttrEndurance.HealthBonus
				returnString += "  +%s block chance\n" % '{:.0%}'.format(AttrEndurance.BlockBonus)
				
			case AttributeType.STRENGTH:
				returnString += "Strength\n"
				returnString += "  +%d damage\n" % AttrStrength.DamageBonus
				returnString += "  +%s critical damage\n" % '{:.0%}'.format(AttrStrength.CritDamageBonus)
				returnString += "  +%s parry chance\n" % '{:.0%}'.format(AttrStrength.ParryBonus)
				
			case _:
				pass
				# Todo, print warning
		
		return returnString
	
	@staticmethod
	def GetAllDescriptions(bIncludeSpacers = True):
		returnString = ""
		
		returnString += AttributeType.GetDescription(AttributeType.AGILITY)
		if bIncludeSpacers: returnString += "\n"
		
		returnString += AttributeType.GetDescription(AttributeType.INTELLECT)
		if bIncludeSpacers: returnString += "\n"
		
		returnString += AttributeType.GetDescription(AttributeType.ENDURANCE)
		if bIncludeSpacers: returnString += "\n"
		
		returnString += AttributeType.GetDescription(AttributeType.STRENGTH)
		if bIncludeSpacers: returnString += "\n"
		
		return returnString
		
class Attribute(Reporter):
	INFO = False
	def __init__(self, owner=None, agility=0, intellect=0, endurance=0, strength=0):
		super().__init__()
		self.owner = owner
		self.agility = agility
		self.intellect = intellect
		self.endurance = endurance
		self.strength = strength
		
	def getStat(self, eStat):
		returnValue = 0
	
		match eStat:
			case StatType.HEALTH_MAX:
				returnValue = self.endurance * AttrEndurance.HealthBonus
			case StatType.DAMAGE:
				returnValue = self.strength * AttrStrength.DamageBonus
				
			case StatType.ARMOR:
				returnValue = 0
				
			case StatType.FIRSTSTRIKES:
				returnValue = 0
			
			case StatType.MULTISTRIKECHANCE:
				returnValue = self.agility * AttrAgility.MultiStrikeBonus
			
			case StatType.DODGECHANCE:
				returnValue = self.agility * AttrAgility.DodgeBonus
				
			case StatType.CRITCHANCE:
				returnValue = self.intellect * AttrIntellect.CritBonus
				
			case StatType.CRITDAMAGEBONUS:
				returnValue = self.strength * AttrStrength.CritDamageBonus
				
			case StatType.PARRYCHANCE:
				returnValue = self.strength * AttrStrength.ParryBonus
				
			case StatType.BLOCKCHANCE:
				returnValue = self.endurance * AttrEndurance.BlockBonus
				
			case _:
				# Todo, add warning
				pass
				
		Attribute.info("Attribute, got %s %s for %s" % (returnValue, eStat, self.owner))
			
		return returnValue
		
	def getAttribute(self, eAttribute):
		returnValue = 0
		
		match eAttribute:
			case AttributeType.AGILITY:
				returnValue = self.agility
				
			case AttributeType.INTELLECT:
				returnValue = self.intellect
				
			case AttributeType.ENDURANCE:
				returnValue = self.endurance
				
			case AttributeType.STRENGTH:
				returnValue = self.strength
				
			case _:
				# Todo, add warning
				pass
				
		Attribute.info("Attribute, got %s %s for %s" % (returnValue, eAttribute, self.owner))
			
		return returnValue
		
	def reset(self):
		self.agility   = 0
		self.intellect = 0
		self.endurance = 0
		self.strength  = 0
		
	def getString_all(self):
		returnString = ""
		
		returnString += "  Agility:   %d\n" % self.agility
		returnString += "  Intellect: %d\n" % self.intellect
		returnString += "  Endurance: %d\n" % self.endurance
		returnString += "  Strength:  %d\n" % self.strength
		
		return returnString